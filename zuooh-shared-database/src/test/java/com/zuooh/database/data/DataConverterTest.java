package com.zuooh.database.data;

import junit.framework.TestCase;

public class DataConverterTest extends TestCase {
   
   private static enum DataEnum{
      X,
      Y,
      Z;
   }
   
   public void testConverter() throws Exception {
      DataConverter converter = new DataConverter();

      // string to number
      assertEquals(converter.convert(Integer.class, "123"), 123);
      assertEquals(converter.convert(Long.class, "123"), 123l);
      assertEquals(converter.convert(Double.class, "123"), 123d);
      assertEquals(converter.convert(Short.class, "123"), (short)123);
      assertEquals(converter.convert(Float.class, "123"), 123f);
      
      // number to number
      assertEquals(converter.convert(Integer.class, 123), 123);
      assertEquals(converter.convert(Long.class, 123), 123l);
      assertEquals(converter.convert(Double.class, 123), 123d);
      assertEquals(converter.convert(Short.class, 123), (short)123);
      assertEquals(converter.convert(Float.class, 123), 123f);
      
      // string to enum
      assertEquals(converter.convert(DataEnum.class, "X"), DataEnum.X);
      assertEquals(converter.convert(DataEnum.class, "Y"), DataEnum.Y);
      assertEquals(converter.convert(DataEnum.class, "Z"), DataEnum.Z);
      
      // enum to string
      assertEquals(converter.convert(String.class, DataEnum.X), "X");
      assertEquals(converter.convert(String.class, DataEnum.Y), "Y");
      assertEquals(converter.convert(String.class, DataEnum.Z), "Z");
   }

}
