package com.zuooh.database.function;

import com.zuooh.database.Column;

public interface DefaultValue {
   Comparable getDefault(Column column, Comparable value);
   DefaultFunction getFunction();
   String getExpression();
}
