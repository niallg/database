package com.zuooh.database;

public interface Tracer<T> {
   void trace(T value, long duration);
}
