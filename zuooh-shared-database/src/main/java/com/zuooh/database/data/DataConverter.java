package com.zuooh.database.data;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DataConverter {
   
   private final Map<String, DataTransform> cache;
   private final DataSupport support;
   
   public DataConverter() {
      this(Collections.EMPTY_LIST);
   }
   
   public DataConverter(List<DataTransform> transforms) {
      this.cache = new ConcurrentHashMap<String, DataTransform>();
      this.support = new DataSupport(transforms);     
   }

   public Comparable convert(DataType type, Comparable value) {
      if(value != null) {
         Class actual = value.getClass();
         Class expect = type.getType();
         
         if(actual == expect) {
            return value;
         }
         return convert(expect, value);
      }
      return null;
      
   }
   
   public Comparable convert(Class type, Comparable value) {
      if(value != null) {
         Class actual = value.getClass();
         
         if(actual == type) {
            return value;
         }
         String to = type.getSimpleName();
         String from = actual.getSimpleName();
         String key = to.concat(from);
         DataTransform transform = cache.get(key);
         
         if(transform == null) {
            transform = match(type, value);
            cache.put(key, transform);
         }         
         return transform.transform(type, value);
      }
      return null;
      
   }
   
   private DataTransform match(Class type, Comparable value) {
      List<DataTransform> transforms = support.getTransforms();
      Class actual = value.getClass();
      
      for(DataTransform transform : transforms) {
         if(transform.accept(value)) {
            Object result = transform.transform(type, value);
            
            if(result != null) {               
               return transform;
            }                  
         }
      }
      throw new IllegalArgumentException("No conversion between " + type + " and " + actual + " possible");
      
   }

}
