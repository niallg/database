package com.zuooh.database.data;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DataSupport {
   
   private List<DataTransform> transforms;

   public DataSupport() {
      this(Collections.EMPTY_LIST);
   }
   
   public DataSupport(List<DataTransform> transforms) {
      this.transforms = transforms;
   }
   
   public List<DataTransform> getTransforms() {
      if(transforms.isEmpty()) {
         transforms = Arrays.<DataTransform>asList(
               new StringTransform(),
               new NumberTransform(),
               new BooleanTransform(),
               new EnumTransform(),
               new CharacterTransform(),               
               new DateTransform()            
         );
      }
      return transforms;
   }
   
}
