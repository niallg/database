package com.zuooh.database.data;

public class CharacterTransform extends DataTransform<Character> {
   
   public boolean accept(Comparable value) {
      if(value != null) {
         Class actual = value.getClass();
         
         if(actual == Character.class) {
            return true;
         }
         if(actual == char.class) {
            return true;
         }
      }
      return false;
   }

   @Override
   public Comparable transform(Class type, Character value) {
      if(value != null) {
         if(type == Character.class) {
            return value;
         }
         if(type == char.class) {
            return value;
         }
         if(type == String.class) {
            return String.valueOf(value);
         } 
      }
      return null;
   }
}
