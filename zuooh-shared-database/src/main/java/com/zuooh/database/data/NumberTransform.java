package com.zuooh.database.data;

import java.util.Date;

public class NumberTransform extends DataTransform<Number> {
   
   public boolean accept(Comparable value) {
      if(value != null) {
         Class actual = value.getClass();               
         Class convert = convert(actual);   
         
         if(convert == Long.class) {
            return true;
         }
         if(convert == Integer.class) {
            return true;
         }
         if(convert == Double.class) {
            return true;
         }
         if(convert == Float.class) {
            return true;
         }
         if(convert == Short.class) {
            return true;
         }
         if(convert == Byte.class) {
            return true;
         }         
      }
      return false;
   }

   @Override
   public Comparable transform(Class type, Number value) {
      if(value != null) {
         Class actual = value.getClass();
         
         if(type == actual) {
            return (Comparable)value;
         }
         Class convert = convert(type);
         
         if(convert == String.class) {
            return String.valueOf(value);
         }
         if (convert == Integer.class) {
            return value.intValue();
         }
         if (convert == Double.class) {
            return value.doubleValue();
         }
         if (convert == Float.class) {
            return value.floatValue();
         }
         if (convert == Byte.class) {
            return value.byteValue();
         }
         if (convert == Short.class) {
            return value.shortValue();
         }
         if (convert == Long.class) {
            return value.longValue();
         } 
         if(convert == Date.class) {
            return DateParser.toDate(value);
         }         
      }
      return null;
   }
}
