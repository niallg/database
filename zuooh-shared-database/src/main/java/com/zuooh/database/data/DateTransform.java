package com.zuooh.database.data;

import java.util.Date;

public class DateTransform extends DataTransform<Date> {

   @Override
   public boolean accept(Comparable value) {
      if(value != null) {
         Class actual = value.getClass();
         
         if(actual == Date.class) {
            return true;
         }
      }
      return false;
   }

   @Override
   public Comparable transform(Class type, Date value) {
      if(value != null) {
         Class actual = value.getClass();
         
         if(type == actual) {
            return value;
         }
         if(type == String.class) {
            return DateParser.toString(value);
         }
         if(type == Long.class) {
            return DateParser.toLong(value);
         }         
      }
      return null;
   }
}
