package com.zuooh.database.data;

public class BooleanTransform extends DataTransform<Boolean> {

   public boolean accept(Comparable value) {
      if(value != null) {
         Class actual = value.getClass();
         
         if(actual == Boolean.class) {
            return true;
         }
         if(actual == boolean.class) {
            return true;
         }
      }
      return false;
   }

   @Override
   public Comparable transform(Class type, Boolean value) {
      if(value != null) {
         if(type == Boolean.class) {
            return value;
         }
         if(type == boolean.class) {
            return value;
         }
         if(type == String.class) {
            return String.valueOf(value);
         } 
      }
      return null;
   }
}
