package com.zuooh.database.data;

public enum DataConstraint {
   REQUIRED("required", 'R'),
   OPTIONAL("optional", 'O'),
   UNIQUE("unique", 'U'),
   KEY("key", 'K');
   
   public final String name;
   public final char code;
   
   private DataConstraint(String name, char code) {
      this.name = name;
      this.code = code;
   }
   
   public String getName() {
      return name;
   }
   
   public char getCode() {
      return code;
   }     
   
   public static DataConstraint resolveConstraint(char code) {
      DataConstraint[] types = values();
      
      for(DataConstraint type : types){
         if(type.code == code) {
            return type;
         }
      }
      throw new IllegalArgumentException("No constraint for code '" + code +"'");      
   }
   
   public static DataConstraint resolveConstraint(String token) {
      DataConstraint[] types = values();
      
      for(DataConstraint type : types){
         if(type.name.equalsIgnoreCase(token)) {
            return type;
         }
      }
      throw new IllegalArgumentException("No constraint for '" + token +"'");      
   }
}
