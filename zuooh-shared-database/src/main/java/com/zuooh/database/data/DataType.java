package com.zuooh.database.data;

import java.util.Date;

import com.zuooh.database.Record;
import com.zuooh.database.Statement;

public enum DataType {
   INT(Integer.class, "int", 'I'){
      @Override
      public Comparable getData(Record record, String name) throws Exception {
         return record.getInteger(name);
      }
      @Override
      public void setData(Statement statement, String name, Comparable value) throws Exception {
         statement.set(name, (Integer)value);        
      }      
   }, 
   TEXT(String.class, "text", 'T'){
      @Override
      public Comparable getData(Record record, String name) throws Exception {
         return record.getString(name);
      }
      @Override
      public void setData(Statement statement, String name, Comparable value) throws Exception {
         statement.set(name, (String)value);        
      }  
   },
   SYMBOL(String.class, "symbol", 'S'){
      @Override
      public Comparable getData(Record record, String name) throws Exception {
         return record.getString(name);
      }
      @Override
      public void setData(Statement statement, String name, Comparable value) throws Exception {
         statement.set(name, (String)value);        
      }  
   },   
   DOUBLE(Double.class, "double", 'E'){
      @Override
      public Comparable getData(Record record, String name) throws Exception {
         return record.getDouble(name);
      }
      @Override
      public void setData(Statement statement, String name, Comparable value) throws Exception {
         statement.set(name, (Double)value);        
      }  
   },
   FLOAT(Float.class, "float", 'F'){
      @Override
      public Comparable getData(Record record, String name) throws Exception {
         return record.getFloat(name);
      }
      @Override
      public void setData(Statement statement, String name, Comparable value) throws Exception {
         statement.set(name, (Float)value);        
      }  
   }, 
   LONG(Long.class, "long", 'J'){
      @Override
      public Comparable getData(Record record, String name) throws Exception {
         return record.getLong(name);
      }
      @Override
      public void setData(Statement statement, String name, Comparable value) throws Exception {
         statement.set(name, (Long)value);        
      }  
   }, 
   SHORT(Short.class, "short", 'H'){
      @Override
      public Comparable getData(Record record, String name) throws Exception {
         return record.getShort(name);
      }
      @Override
      public void setData(Statement statement, String name, Comparable value) throws Exception {
         statement.set(name, (Short)value);        
      }  
   }, 
   BYTE(Byte.class, "byte", 'X'){
      @Override
      public Comparable getData(Record record, String name) throws Exception {
         return record.getByte(name);
      }
      @Override
      public void setData(Statement statement, String name, Comparable value) throws Exception {
         statement.set(name, (Byte)value);        
      }  
   }, 
   BOOLEAN(Boolean.class, "boolean", 'B'){
      @Override
      public Comparable getData(Record record, String name) throws Exception {
         return record.getBoolean(name);
      }
      @Override
      public void setData(Statement statement, String name, Comparable value) throws Exception {
         statement.set(name, (Boolean)value);        
      }  
   }, 
   CHAR(Character.class, "char", 'C'){
      @Override
      public Comparable getData(Record record, String name) throws Exception {
         return record.getCharacter(name);
      }
      @Override
      public void setData(Statement statement, String name, Comparable value) throws Exception {
         statement.set(name, (Character)value);        
      }  
   },
   DATE(Long.class, "date", 'D') {
      @Override
      public Comparable getData(Record record, String name) throws Exception {
         return record.getLong(name);
      }
      @Override
      public void setData(Statement statement, String name, Comparable value) throws Exception {
         statement.set(name, (Long)value);        
      }  
   };
   
   public final String name;
   public final Class type;
   public final char code;
   
   private DataType(Class type, String name, char code) {    
      this.code = code;
      this.name = name;
      this.type = type;
   }
   
   public Class getType() {
      return type;
   }
   
   public String getName() {
      return name;
   }
   
   public char getCode() {
      return code;
   }   
   
   public abstract Comparable getData(Record record, String name) throws Exception;
   public abstract void setData(Statement statement, String name, Comparable value) throws Exception;   
   
   public static DataType resolveType(char code) {
      DataType[] types = values();
      
      for(DataType type : types) {
         if(type.code == code) {
            return type;
         }
      }
      throw new IllegalArgumentException("No type found for code '" + code + "'");
   }   
   
   public static DataType resolveType(String value) {
      DataType[] types = values();
      
      for(DataType type : types) {
         if(type.name.equalsIgnoreCase(value)) {
            return type;
         }
      }
      throw new IllegalArgumentException("No type found for '" + value + "'");
   }   

   public static DataType resolveType(Class type) {
      if (type.equals(int.class)) {
         return INT;
      }
      if (type.equals(double.class)) {
         return DOUBLE;
      }
      if (type.equals(float.class)) {
         return FLOAT;
      }
      if (type.equals(boolean.class)) {
         return BOOLEAN;
      }
      if (type.equals(byte.class)) {
         return BYTE;
      }
      if (type.equals(short.class)) {
         return SHORT;
      }
      if (type.equals(long.class)) {
         return LONG;
      }
      if (type.equals(char.class)) {
         return CHAR;
      }
      if (type.equals(String.class)) {
         return TEXT;
      }
      if (type.equals(Integer.class)) {
         return INT;
      }
      if (type.equals(Double.class)) {
         return DOUBLE;
      }
      if (type.equals(Float.class)) {
         return FLOAT;
      }
      if (type.equals(Boolean.class)) {
         return BOOLEAN;
      }
      if (type.equals(Byte.class)) {
         return BYTE;
      }
      if (type.equals(Short.class)) {
         return SHORT;
      }
      if (type.equals(Long.class)) {
         return LONG;
      }
      if (type.equals(Character.class)) {
         return CHAR;
      }
      if (type.equals(Date.class)) {
         return DATE;
      }
      if (Enum.class.isAssignableFrom(type)) {
         return SYMBOL;
      }
      return TEXT;
   }
}
