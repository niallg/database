package com.zuooh.database.data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateParser {

   private static final ThreadLocal<DateFormat> LONG_FORMAT = new ThreadLocal<DateFormat>() {

      @Override
      protected DateFormat initialValue() {
         return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
      }
   };
   
   private static final ThreadLocal<DateFormat> SHORT_FORMAT = new ThreadLocal<DateFormat>() {

      @Override
      protected DateFormat initialValue() {
         return new SimpleDateFormat("yyyy-MM-dd");
      }
   };
   
   public static String toString(Long date) {      
      try {         
         DateFormat format = LONG_FORMAT.get();
         
         if(format != null) {
            return format.format(date);
         }
         return date.toString();
      } catch (Exception e) {
         throw new IllegalStateException("Unable to format '" + date + "'", e);
      }
   }
   
   public static String toString(Date date) {      
      try {         
         DateFormat format = LONG_FORMAT.get();
         
         if(format != null) {
            return format.format(date);
         }
         return date.toString();
      } catch (Exception e) {
         throw new IllegalStateException("Unable to format '" + date + "'", e);
      }
   }
   
   public static Date toDate(Number value) {      
      try {         
         Long time = value.longValue();
         return new Date(time);
      } catch (Exception e) {
         throw new IllegalStateException("Unable to format '" + value + "'", e);
      }
   }
   
   public static Long toLong(Date date) {      
      try {         
         return date.getTime();
      } catch (Exception e) {
         throw new IllegalStateException("Unable to format '" + date + "'", e);
      }
   }

   public static Date toDate(String value) {      
      try {         
         DateFormat format = toFormat(value);
         Date date = format.parse(value);
         
         return date;
      } catch (Exception e) {
         throw new IllegalStateException("Unable to parse '" + value + "'", e);
      }
   }
   
   public static Long toLong(String value) {      
      try {         
         DateFormat format = toFormat(value);
         Date date = format.parse(value);
         
         return date.getTime();
      } catch (Exception e) {
         throw new IllegalStateException("Unable to parse '" + value + "'", e);
      }
   }
   
   public static DateFormat toFormat(String value) {
      try {
         int length = value.length();
         
         if(length > 10) {
            return LONG_FORMAT.get();
         }
         return SHORT_FORMAT.get();
      } catch(Exception e) {
         throw new IllegalStateException("Unable to find format for '" + value + "'", e);
      }
   }
}
