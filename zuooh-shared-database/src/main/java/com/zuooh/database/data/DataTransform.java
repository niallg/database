package com.zuooh.database.data;

public abstract class DataTransform<T> {

   protected Class convert(Class type) {
      if (type == int.class) {
         return Integer.class;
      }
      if (type == double.class) {
         return Double.class;
      }
      if (type == float.class) {
         return Float.class;
      }
      if (type == boolean.class) {
         return Boolean.class;
      }
      if (type == byte.class) {
         return Byte.class;
      }
      if (type == short.class) {
         return Short.class;
      }
      if (type == long.class) {
         return Long.class;
      }
      if (type == char.class) {
         return Character.class;
      }
      return type;
   }
   
   public abstract Comparable transform(Class type, T value);
   public abstract boolean accept(Comparable value);   
}
