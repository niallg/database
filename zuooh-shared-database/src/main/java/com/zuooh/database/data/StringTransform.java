package com.zuooh.database.data;

import java.util.Date;

import com.zuooh.attribute.ClassResolver;
import com.zuooh.attribute.StringConverter;

public class StringTransform extends DataTransform<String> {
   
   private final StringConverter converter;
   private final ClassResolver resolver;
   
   public StringTransform() {
      this.resolver = new ClassResolver();
      this.converter = new StringConverter(resolver);
   }
   
   @Override
   public boolean accept(Comparable value) {
      if(value != null) {
         Class actual = value.getClass();
         
         if(actual == String.class) {
            return true;
         }
      }
      return false;
   }

   @Override
   public Comparable transform(Class type, String value) {
      if(value != null) {
         Class actual = value.getClass();
         
         if(type == actual) {
            return value;
         }
         if(type == Date.class) {
            return DateParser.toDate(value);
         } 
         try {
            return (Comparable)converter.convert(type, value);
         } catch(Exception e) {
            throw new IllegalStateException("Could not convert '" + value + "' to " + type);
         }
      }
      return null;
   }
}
