package com.zuooh.database.data;

public class EnumTransform extends DataTransform<Enum> {

   @Override
   public boolean accept(Comparable value) {
      if(value != null) {
         Class actual = value.getClass();
         Class parent = actual.getSuperclass();
         
         if(actual == Enum.class) {
            return true;
         }
         if(parent == Enum.class) {
            return true;
         }
      }
      return false;
   }

   @Override
   public Comparable transform(Class type, Enum value) {
      if(value != null) {
         Class actual = value.getClass();
         
         if(type == actual) {
            return value;
         }
         if(type == String.class) {
            return value.name();
         }       
      }
      return null;
   }
}
