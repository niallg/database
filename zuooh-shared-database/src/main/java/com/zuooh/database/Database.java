package com.zuooh.database;

public interface Database {
   DatabaseConnection getConnection() throws Exception;
}
