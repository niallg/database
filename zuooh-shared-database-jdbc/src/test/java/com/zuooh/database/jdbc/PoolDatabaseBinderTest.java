package com.zuooh.database.jdbc;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import org.h2.jdbcx.JdbcDataSource;
import org.simpleframework.xml.core.Persister;

import com.zuooh.database.DatabaseConnection;
import com.zuooh.database.bind.TableBinder;
import com.zuooh.database.bind.load.TableDefinition;
import com.zuooh.database.bind.table.TableBuilder;
import com.zuooh.database.bind.table.attribute.AttributeTableBuilder;
import com.zuooh.database.bind.table.statement.CreateStatement;
import com.zuooh.database.bind.table.statement.DropStatement;
import com.zuooh.database.build.dom.element.TableBinderElement;
import com.zuooh.database.build.dom.schema.TableBinderBuilder;
import com.zuooh.database.sql.compile.QueryCompiler;

public class PoolDatabaseBinderTest extends TestCase {
   
   private static final String SOURCE =
   "<table name=\"test\" type=\"com.zuooh.database.jdbc.PoolDatabaseBinderTest$SomePerson\">\r\n" +
   "  <schema>\r\n"+
   "    <key>\r\n" +
   "      <column name=\"mail\" />\r\n" +
   "    </key>\r\n" +
   "    <!--column name=\"time\" type=\"date\" default=\"time\"/-->\r\n" +
   "  </schema>\r\n" +
   "  <insert>\r\n" +
   "    <row>\r\n" +
   "      <value column=\"name\">John Doe</value>\r\n" +
   "      <value column=\"age\">22</value>\r\n" +
   "      <value column=\"addressNumber\">11</value>\r\n" +   
   "      <value column=\"addressHome\">Home</value>\r\n" +
   "      <value column=\"addressStreet\">Some Street</value>\r\n" +
   "      <value column=\"addressCity\">Sin City</value>\r\n" +
   "      <value column=\"addressCountry\">Atlantis</value>\r\n" +
   "      <value column=\"jobTitle\">Mayor</value>\r\n" +
   "      <value column=\"mail\">john.doe@yahoo.com</value>\r\n" +
   "    </row>\r\n" +
   "    <row>\r\n" +
   "      <value column=\"name\">Tom Waters</value>\r\n" +
   "      <value column=\"age\">32</value>\r\n" +
   "      <value column=\"addressNumber\">31</value>\r\n" +
   "      <value column=\"addressHome\">Apartment</value>\r\n" +
   "      <value column=\"addressStreet\">Blah Street</value>\r\n" +
   "      <value column=\"addressCity\">A City</value>\r\n" +
   "      <value column=\"addressCountry\">Ireland</value>\r\n" +
   "      <value column=\"jobTitle\">Software Engineer</value>\r\n" +
   "      <value column=\"mail\">tomw@gmail.com</value>\r\n" +
   "    </row>\r\n" +   
   "  </insert>\r\n" +
   "</table>\r\n";
         
   
   private static class SomeAddress implements Serializable {
      int number;
      String home;
      String street;
      String city;
      String country;
   }
   
   private static class SomePerson implements Serializable {
      String name;
      int age;
      SomeAddress address;
      String jobTitle;
      String mail;
   }
   
   public void testDatabaseCreation() throws Exception {
      Persister persister = new Persister();
      TableBinderBuilder builder = persister.read(TableBinderBuilder.class, SOURCE);
      JdbcDataSource source = new JdbcDataSource();
      
      source.setUrl("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DATABASE_TO_UPPER=false");
      
      Map<String, String> translations = new HashMap<String, String>();
      QueryCompiler compiler = new QueryCompiler(translations);      
      
      translations.put("optional", null);
      translations.put("required", "not null");
      translations.put("sequence", "default sequence");
      translations.put("text",  "varchar(100)");
      translations.put("insert or ignore",  "insert");      
            
      PoolDatabase database = new PoolDatabase(source, compiler);  
      TableBuilder tableBuilder = new AttributeTableBuilder(database);
      List<String> script = new LinkedList<String>();
      TableBinderElement tableElement = builder.createElement();
      TableDefinition tableDefinition = tableElement.createTable(tableBuilder);
      TableBinder tableBinder = tableDefinition.createBinder(tableBuilder, script);
      DropStatement drop = tableBinder.dropIfExists();
      String dropStatement = drop.compile();
      
      System.err.println(dropStatement); 
      
      CreateStatement create = tableBinder.create();
      String createStatement = create.compile();
      
      System.err.println(createStatement);
      
      drop.execute();
      create.execute();
      
      for(String line : script) {
         DatabaseConnection connection = database.getConnection();
         try {
            System.err.println(line);
            connection.executeStatement(line);
         } finally {
            connection.closeConnection();
         }
      }           
      System.err.println(SOURCE);
      System.err.println();
   }

}
