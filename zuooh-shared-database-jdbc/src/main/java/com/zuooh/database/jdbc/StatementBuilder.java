package com.zuooh.database.jdbc;

import static java.util.Collections.EMPTY_LIST;

import java.sql.Connection;

import com.zuooh.database.Record;
import com.zuooh.database.RecordIterator;
import com.zuooh.database.ResultCache;
import com.zuooh.database.ResultIterator;
import com.zuooh.database.StatementTemplate;
import com.zuooh.database.sql.Query;
import com.zuooh.database.sql.compile.QueryCompiler;

public class StatementBuilder extends StatementTemplate {
   
   protected static final Object[] EMPTY = {};
   
   protected final StatementTracer tracer;
   protected final QueryCompiler compiler;
   protected final Connection connection;
   protected final ResultCache cache;
   protected final Query query;
   
   public StatementBuilder(Connection connection, StatementTracer tracer, ResultCache cache, QueryCompiler compiler, Query query) {
      this.connection = connection;
      this.compiler = compiler;
      this.tracer = tracer;
      this.cache = cache;
      this.query = query;   
   }
   
   @Override
   public synchronized ResultIterator<Record> execute() throws Exception {
      long start = System.currentTimeMillis();
      boolean disposed = closed.get();
      
      if(disposed) {
         throw new IllegalStateException("This statement has been closed");
      }    
      String table = query.getTable();
      String expression = compiler.compile(query, EMPTY);
      java.sql.Statement statement = connection.createStatement();      
      
      try {
         try {
            statement.execute(expression);
            cache.clear(table);
         } finally {
            statement.close();
         }   
         return new RecordIterator(EMPTY_LIST, expression);
      } finally {
         long finish = System.currentTimeMillis();
         long duration = finish - start;
         
         tracer.traceStatement(expression, duration);
      }
   } 
}
