package com.zuooh.database.jdbc;

import static com.zuooh.database.sql.ParameterType.NAME;
import static com.zuooh.database.sql.ParameterType.VALUE;
import static java.util.Collections.EMPTY_LIST;

import java.sql.Connection;
import java.util.List;

import com.zuooh.database.Record;
import com.zuooh.database.RecordIterator;
import com.zuooh.database.ResultCache;
import com.zuooh.database.ResultIterator;
import com.zuooh.database.sql.Condition;
import com.zuooh.database.sql.Parameter;
import com.zuooh.database.sql.ParameterType;
import com.zuooh.database.sql.Query;
import com.zuooh.database.sql.WhereClause;
import com.zuooh.database.sql.compile.QueryCompiler;

public class UpdateStatementBuilder extends StatementBuilder {
   
   public UpdateStatementBuilder(Connection connection, StatementTracer tracer, ResultCache cache, QueryCompiler compiler, Query query) {
      super(connection, tracer, cache, compiler, query);
   }
   
   @Override
   public synchronized ResultIterator<Record> execute() throws Exception {
      boolean disposed = closed.get();
      
      if(disposed) {
         throw new IllegalStateException("This statement has been closed");
      }
      WhereClause clause = query.getWhereClause();
      List<Condition> conditions = clause.getConditions();
      List<Parameter> parameters = query.getParameters();
      int conditionCount = conditions.size();
      int parameterCount = parameters.size();
      int totalCount = parameterCount + conditionCount;
      
      if(totalCount > 0) {
         Object[] list = new Object[totalCount];
         
         for(int i = 0; i < parameterCount; i++) {
            Parameter parameter = parameters.get(i);
            String name = parameter.getName();
            ParameterType type = parameter.getType();
            
            if(type == VALUE) {
               list[i] = parameter.getValue();
            } else if(type == NAME) {
               list[i] = attributes.get(name);  
            } else {
               throw new IllegalStateException("Parameter '" + parameter + "' is not a named or literal parameter");            
            }        
         }
         for(int i = 0; i < conditionCount; i++) {
            Condition condition = conditions.get(i);
            Parameter parameter = condition.getParameter();
            String name = parameter.getName();
            ParameterType type = parameter.getType();
            
            if(type == VALUE) {
               list[i + parameterCount] = parameter.getValue();
            } else if(type == NAME) {
               list[i + parameterCount] = attributes.get(name);  
            } else {
               throw new IllegalStateException("Parameter '" + parameter + "' is not a named or literal parameter");            
            } 
         }
         return execute(list);
      }
      return execute(EMPTY);
   }

   protected synchronized ResultIterator<Record> execute(Object[] values) throws Exception {
      long start = System.currentTimeMillis();
      boolean disposed = closed.get();
      
      if(disposed) {
         throw new IllegalStateException("This statement has been closed");
      }    
      String table = query.getTable();
      String expression = compiler.compile(query, values);
      java.sql.Statement statement = connection.createStatement();
      
      try {         
         try {
            statement.execute(expression);
            cache.clear(table);
         } finally {
            statement.close();
         }   
         return new RecordIterator(EMPTY_LIST, expression);
      } finally {
         long finish = System.currentTimeMillis();
         long duration = finish - start;
         
         tracer.traceStatement(expression, duration);
      }
   }
}
