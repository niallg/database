package com.zuooh.database.imdb.io.write;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.zuooh.database.imdb.OperationType;
import com.zuooh.database.imdb.io.FilePointer;
import com.zuooh.database.imdb.io.write.ChangeRecord;
import com.zuooh.database.imdb.io.write.ChangeRecordBatch;
import com.zuooh.database.imdb.io.write.DeleteRecordWriter;
import com.zuooh.database.imdb.io.write.FileLogListener;

public class FileLogListenerTest extends TestCase {   
   
   public void testFileLogListener() throws Exception {
      String tempDir = System.getProperty("java.io.tmpdir");
      String name = FileLogListenerTest.class.getSimpleName()+System.currentTimeMillis();
      FilePointer pointer = new FilePointer(tempDir, name);
      FileLogListener listener = new FileLogListener(pointer, "origin", 10);
      List<ChangeRecord> records = new ArrayList<ChangeRecord>();
      ChangeRecordBatch batch = new ChangeRecordBatch(records, "origin", "table");
      
      for(int i = 0; i < 100; i++){
         DeleteRecordWriter writer = new DeleteRecordWriter("origin", "key-"+i);
         ChangeRecord record = new ChangeRecord(writer, OperationType.DELETE, "origin", "table");
         records.add(record);
      }
      listener.update(batch);
   }

}
