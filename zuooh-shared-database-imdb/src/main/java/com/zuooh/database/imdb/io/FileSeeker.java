package com.zuooh.database.imdb.io;

public interface FileSeeker {
   boolean accept(FilePath path);
}
