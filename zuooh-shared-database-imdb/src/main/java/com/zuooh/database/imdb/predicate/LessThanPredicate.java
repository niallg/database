package com.zuooh.database.imdb.predicate;

import com.zuooh.database.imdb.Cell;
import com.zuooh.database.imdb.Row;

public class LessThanPredicate extends Predicate {
   
   private final Comparable right;
   private final String name;
   private final int index;
   
   public LessThanPredicate(Comparable right, String name, int index) {
      this.index = index;
      this.right = right;
      this.name = name;
   }
   
   @Override
   public boolean accept(Row tuple) {
      Cell cell = tuple.getCell(index);
      Comparable left = cell.getValue();         
    
      if(left == null || right == null) {
         return false;
      }
      return left.compareTo(right) < 0;
   }
   
   @Override
   public String toString() {
      return String.format("%s < %s", name, right);
   }   
}