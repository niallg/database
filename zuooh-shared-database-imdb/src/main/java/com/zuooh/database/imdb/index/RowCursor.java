package com.zuooh.database.imdb.index;

import java.util.Iterator;

import com.zuooh.database.imdb.Row;

public interface RowCursor extends Iterable<Row>{
   Iterator<Row> iterator();
   int count();
}
