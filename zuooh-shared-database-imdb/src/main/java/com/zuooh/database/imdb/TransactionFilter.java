package com.zuooh.database.imdb;

import com.zuooh.database.imdb.Transaction;

public interface TransactionFilter {
   boolean accept(Transaction transaction);
}
