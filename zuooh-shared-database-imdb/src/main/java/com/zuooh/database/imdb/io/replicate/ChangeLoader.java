package com.zuooh.database.imdb.io.replicate;

import com.zuooh.database.imdb.TransactionFilter;
import com.zuooh.database.imdb.io.DataRecordConsumer;
import com.zuooh.database.imdb.io.DataRecordIterator;
import com.zuooh.database.imdb.io.FileBlockConsumer;
import com.zuooh.database.imdb.io.FileSeeker;
import com.zuooh.database.imdb.io.TimeFileSeeker;
import com.zuooh.database.imdb.io.read.ChangeProcessor;
import com.zuooh.database.imdb.io.read.ChangeScheduler;

public class ChangeLoader {  
   
   private final ChangeProcessor processor;
   private final TransactionFilter filter;
   private final String directory;  

   public ChangeLoader(ChangeScheduler executor, Position position, String origin, String directory) { 
      this.filter = new RestoreFilter(position, origin); 
      this.processor = new ChangeProcessor(executor, filter, true);
      this.directory = directory;
   }   
   
   public int process() throws Exception {
      FileSeeker filter = new TimeFileSeeker();
      DirectoryCleaner cleaner = new DirectoryCleaner(directory);
      FileBlockConsumer consumer = new FileBlockConsumer(filter, directory);
      DataRecordConsumer source = new DataRecordConsumer(consumer);
      DataRecordIterator iterator = new DataRecordIterator(source);

      try {
         cleaner.clean(); // remove old empty files
         consumer.start();
         return processor.process(iterator);
      } finally {
         consumer.stop();
      }
   }

}
