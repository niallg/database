package com.zuooh.database.imdb.statement;

import java.util.Collections;
import java.util.List;

import com.zuooh.database.Column;
import com.zuooh.database.Record;
import com.zuooh.database.RecordIterator;
import com.zuooh.database.ResultIterator;
import com.zuooh.database.Schema;
import com.zuooh.database.imdb.Catalog;
import com.zuooh.database.imdb.Table;
import com.zuooh.database.imdb.TableModel;
import com.zuooh.database.sql.Query;

public class CreateIndexExecutor extends StatementExecutor {
   
   private final Catalog catalog;
   private final Query query;
   
   public CreateIndexExecutor(Catalog catalog, Query query) {
      this.catalog = catalog;
      this.query = query;
   }
   
   @Override
   public ResultIterator<Record> execute() throws Exception {
      boolean disposed = closed.get();
      
      if(disposed) {
         throw new IllegalStateException("This statement has been closed");
      }
      String name = query.getTable();
      String index = query.getName();
      String expression = query.getSource();
      Table table = catalog.findTable(name);
      
      if(table == null) {
         throw new IllegalStateException("Unable to create index '" + index + "' as table '" + table + "' does not exist");
      }
      List<String> columns = query.getColumns();
      Schema schema = table.getSchema();
      TableModel tableModel = table.getModel();
      int columnCount = columns.size();
      
      for(int i = 0; i < columnCount; i++) {
         String columnName = columns.get(i);
         Column column = schema.getColumn(columnName);
         
         if(table == null) {
            throw new IllegalStateException("Unable to create index '" + index + "' as column '" + columnName + "' does not exist");
         }
         tableModel.index(column);
      }
      return new RecordIterator(Collections.EMPTY_LIST, expression);      
   }
}
