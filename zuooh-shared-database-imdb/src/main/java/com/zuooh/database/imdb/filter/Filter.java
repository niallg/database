package com.zuooh.database.imdb.filter;

import java.util.Iterator;

import com.zuooh.database.imdb.Row;
import com.zuooh.database.imdb.index.RowSeries;

public interface Filter {
   Iterator<Row> select(RowSeries series);
   int count(RowSeries series);
}
