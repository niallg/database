package com.zuooh.database.imdb.statement;

import java.util.Collections;

import com.zuooh.database.Record;
import com.zuooh.database.RecordIterator;
import com.zuooh.database.ResultIterator;
import com.zuooh.database.imdb.Catalog;
import com.zuooh.database.imdb.Table;
import com.zuooh.database.sql.Query;

public class DropIndexExecutor extends StatementExecutor {
   
   private final Catalog catalog;
   private final Query query;
   
   public DropIndexExecutor(Catalog catalog, Query query) {
      this.catalog = catalog;
      this.query = query;
   }
   
   @Override
   public ResultIterator<Record> execute() throws Exception {
      boolean disposed = closed.get();
      
      if(disposed) {
         throw new IllegalStateException("This statement has been closed");
      }
      String name = query.getTable();
      String index = query.getName();
      String expression = query.getSource();
      Table table = catalog.findTable(name);
      
      if(table == null) {
         throw new IllegalStateException("Unable to drop index '" + index + "' as table '" + table + "' does not exist");
      }
      return new RecordIterator(Collections.EMPTY_LIST, expression);      
   }
}
