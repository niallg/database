package com.zuooh.database.imdb.filter;

import com.zuooh.database.Column;
import com.zuooh.database.Schema;
import com.zuooh.database.imdb.Catalog;
import com.zuooh.database.imdb.Table;
import com.zuooh.database.sql.OrderByClause;
import com.zuooh.database.sql.Query;

public class SortOrderEvaluator {

   private final Catalog catalog;
   private final Query query;

   public SortOrderEvaluator(Catalog catalog, Query query) {   
      this.catalog = catalog;
      this.query = query;
   }
   
   public SortComparator evaluateOrder() {
      OrderByClause order = query.getOrderByClause();
      String direction = order.getDirection();
      String name = order.getColumn();
      
      if(name != null) {
         String reference = query.getTable();
         Table table = catalog.findTable(reference);
         Schema schema = table.getSchema();
         Column column = schema.getColumn(name);
         
         if(direction == null) {
            return new SortComparator(column, true); // default to asc
         }
         if(direction.equalsIgnoreCase("asc")) {
            return new SortComparator(column, true);
         }
         if(direction.equalsIgnoreCase("desc")) {
            return new SortComparator(column, false);
         }
         throw new IllegalArgumentException("Unknown sort order '" + direction + "' for '" + reference + "'");
      }
      return null; 
   }
}
