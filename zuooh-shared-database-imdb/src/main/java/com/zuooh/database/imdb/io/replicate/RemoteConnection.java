package com.zuooh.database.imdb.io.replicate;

import java.io.DataInputStream;
import java.io.DataOutputStream;

public interface RemoteConnection {
   DataOutputStream getOutputStream();
   DataInputStream getInputStream();
}
