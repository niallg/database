package com.zuooh.database.imdb.io.read;

import com.zuooh.database.imdb.Transaction;
import com.zuooh.database.imdb.TransactionFilter;

public class BeginOperation implements ChangeOperation {
   
   private final TransactionFilter filter;
   private final Transaction transaction; 

   public BeginOperation(TransactionFilter filter, Transaction transaction) {
      this.transaction = transaction;
      this.filter = filter;      
   }
   
   @Override
   public boolean execute(ChangeAssembler assembler) {
      String origin = transaction.getOrigin();
      String table = transaction.getTable();
      
      if(filter.accept(transaction)) {
         assembler.onBegin(origin, table, transaction);         
         return true;
      } 
      return false;
   }
}
