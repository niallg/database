package com.zuooh.database.imdb.io.write;

import static com.zuooh.database.imdb.OperationType.DROP;

import java.io.IOException;

import com.zuooh.database.imdb.io.DataRecordCounter;
import com.zuooh.database.imdb.io.DataRecordWriter;

public class DropRecordWriter implements ChangeRecordWriter {
   
   private final String origin;
   
   public DropRecordWriter(String origin) {
      this.origin = origin;     
   }

   @Override
   public void write(DataRecordWriter writer, DataRecordCounter counter) throws IOException {
      writer.writeChar(DROP.code);
      writer.writeString(origin);      
   }
}
