package com.zuooh.database.imdb.io.write;

import static com.zuooh.database.imdb.OperationType.COMMIT;

import java.io.IOException;

import com.zuooh.database.imdb.io.DataRecordCounter;
import com.zuooh.database.imdb.io.DataRecordWriter;

public class CommitRecordWriter implements ChangeRecordWriter {
   
   private final String origin;  
   
   public CommitRecordWriter(String origin) {
      this.origin = origin;     
   }

   @Override
   public void write(DataRecordWriter writer, DataRecordCounter counter) throws IOException {
      writer.writeChar(COMMIT.code);
      writer.writeString(origin);      
   }
}
