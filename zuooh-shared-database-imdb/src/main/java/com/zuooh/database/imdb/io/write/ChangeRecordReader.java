package com.zuooh.database.imdb.io.write;

import java.io.IOException;

import com.zuooh.database.imdb.io.DataRecordReader;
import com.zuooh.database.imdb.io.read.ChangeOperation;

public interface ChangeRecordReader {
   ChangeOperation read(DataRecordReader reader) throws IOException;
}
