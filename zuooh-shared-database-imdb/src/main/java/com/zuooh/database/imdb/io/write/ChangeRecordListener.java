package com.zuooh.database.imdb.io.write;

import java.io.IOException;

public interface ChangeRecordListener {
   void update(ChangeRecordBatch batch) throws IOException;
}
