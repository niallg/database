package com.zuooh.database.imdb.io.read;

import com.zuooh.database.imdb.Transaction;
import com.zuooh.database.imdb.TransactionFilter;
import com.zuooh.database.imdb.io.replicate.Position;

public class RepeatFilter implements TransactionFilter {
   
   private final Position position;
   
   public RepeatFilter() {
      this.position = new Position();
   }

   @Override
   public boolean accept(Transaction transaction) {      
      String table = transaction.getTable();
      long time = transaction.getTime();
      long count = transaction.getSequence();
      long previousTime = position.getTime(table);
      long previousCount = position.getCount(table);
      
      if(time < previousTime) {
         return false;
      }
      if(time > previousTime) {
         position.setTime(table, time);
         position.setCount(table, count);
         return true;
      }
      if(count < previousCount) {
         return false;
      }
      if(count > previousCount) {
         position.setCount(table, count);
         return true;
      }
      return false;
   }

}
