package com.zuooh.database.imdb.io.replicate;

import java.io.File;

import com.zuooh.database.imdb.io.FilePath;
import com.zuooh.database.imdb.io.FileSeeker;

public class PositionSeeker implements FileSeeker {
   
   private final Position position;
   
   public PositionSeeker(Position position) {
      this.position = position;
   }

   @Override
   public boolean accept(FilePath path) {
      File file = path.getFile();
      String name = path.getName();
      long time = path.getTime();
      long start = position.getTime(name);
      
      if(time >= start) {
         return file.exists();
      }      
      return false;
   }

}
