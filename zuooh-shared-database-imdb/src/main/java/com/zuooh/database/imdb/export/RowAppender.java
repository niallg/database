package com.zuooh.database.imdb.export;

import java.io.IOException;

import com.zuooh.database.Column;
import com.zuooh.database.data.DataType;
import com.zuooh.database.imdb.Cell;
import com.zuooh.database.imdb.Row;

public class RowAppender {

   private final ValueEscaper escaper;
   private final Appendable appender;
   
   public RowAppender(ValueEscaper escaper, Appendable appender) {
      this.appender = appender;
      this.escaper = escaper;
   }
   
   public void append(Row row) throws IOException {
      int count = row.getCount();
      
      for(int i = 0; i < count; i++) {
         Cell cell = row.getCell(i);
         Comparable value = cell.getValue();       
         
         if(i > 0) {
            appender.append(',');
         }
         if(value != null) {
            append(cell);
         }
      }
   }
   
   private void append(Cell cell) throws IOException {
      Comparable value = cell.getValue();
      
      if(value != null) {
         Column column = cell.getColumn();
         DataType data = column.getDataType();            
         String text = String.valueOf(value);
         Class type = data.getType();
               
         if(type == String.class) { // SYMBOL and TEXT
            text = escaper.escape(text); 
         }
         appender.append(text);
      }
   }   
}
