package com.zuooh.database.imdb.io.write;

import static com.zuooh.database.imdb.OperationType.DELETE;

import java.io.IOException;

import com.zuooh.database.imdb.io.DataRecordCounter;
import com.zuooh.database.imdb.io.DataRecordWriter;

public class DeleteRecordWriter implements ChangeRecordWriter {
   
   private final String origin;
   private final String key;
   
   public DeleteRecordWriter(String origin, String key) {
      this.origin = origin;
      this.key = key;
   }

   @Override
   public void write(DataRecordWriter writer, DataRecordCounter counter) throws IOException {
      if(key == null) {
         throw new IllegalStateException("Delete does not specify a row");
      }
      writer.writeChar(DELETE.code);
      writer.writeString(origin);
      writer.writeString(key);
   }
}
