package com.zuooh.database.imdb.io;

public interface DataBlockConsumer {
   DataBlock read(long wait);
}
