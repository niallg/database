package com.zuooh.database.imdb.statement;

import com.zuooh.database.StatementTemplate;
import com.zuooh.database.sql.compile.QueryCompiler;

public abstract class StatementExecutor extends StatementTemplate {
   
   protected final QueryCompiler compiler;
   
   protected StatementExecutor() {
      this.compiler = new QueryCompiler();
   }
}
