package com.zuooh.database.imdb.io.read;

public interface ChangeOperation {
   boolean execute(ChangeAssembler assembler);
}
