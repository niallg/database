package com.zuooh.database.imdb.io.write;

import static com.zuooh.database.imdb.OperationType.ROLLBACK;

import java.io.IOException;

import com.zuooh.database.imdb.io.DataRecordCounter;
import com.zuooh.database.imdb.io.DataRecordWriter;

public class RollbackRecordWriter implements ChangeRecordWriter {
   
   private final String origin;  
   
   public RollbackRecordWriter(String origin) {
      this.origin = origin;     
   }

   @Override
   public void write(DataRecordWriter writer, DataRecordCounter counter) throws IOException {
      writer.writeChar(ROLLBACK.code);
      writer.writeString(origin);      
   }
}
