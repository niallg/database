package com.zuooh.database.imdb.index;

import com.zuooh.database.imdb.Row;

public interface ColumnIndexUpdater<T extends Comparable<T>>  extends ColumnIndex<T> {
   void update(Row tuple);
   void remove(Row tuple);
   void clear();
}
