package com.zuooh.database.imdb.filter;

import com.zuooh.database.imdb.index.RowSeries;

public interface FilterNode {
   RowSeries apply(RowSeries series);
}
