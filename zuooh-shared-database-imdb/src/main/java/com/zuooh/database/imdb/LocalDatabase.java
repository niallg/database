package com.zuooh.database.imdb;

import com.zuooh.database.Database;
import com.zuooh.database.DatabaseConnection;
import com.zuooh.database.Statement;
import com.zuooh.database.imdb.statement.StatementExecutorConverter;
import com.zuooh.database.imdb.statement.StatementExecutorTimer;
import com.zuooh.database.sql.QueryConverter;
import com.zuooh.database.sql.build.QueryProcessor;

public class LocalDatabase implements Database {
   
   private final QueryConverter<Statement> converter;
   private final QueryConverter<Statement> timer;
   private final QueryProcessor processor;
   
   public LocalDatabase(Catalog catalog, String origin) {
      this(catalog, origin, false);
   }
   
   public LocalDatabase(Catalog catalog, String origin, boolean debug) {
      this.converter = new StatementExecutorConverter(catalog, origin);
      this.timer = new StatementExecutorTimer(converter, debug);
      this.processor = new QueryProcessor(timer);    
   }
   
   @Override
   public DatabaseConnection getConnection() throws Exception {
      return new LocalDatabaseConnection(processor);  
   }
}
