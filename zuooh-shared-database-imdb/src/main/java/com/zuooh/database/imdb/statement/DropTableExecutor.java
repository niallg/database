package com.zuooh.database.imdb.statement;

import java.util.Collections;

import com.zuooh.database.Record;
import com.zuooh.database.RecordIterator;
import com.zuooh.database.ResultIterator;
import com.zuooh.database.imdb.Catalog;
import com.zuooh.database.sql.Query;

public class DropTableExecutor extends StatementExecutor {
   
   private final Catalog catalog;
   private final Query query;
   private final String origin;
   
   public DropTableExecutor(Catalog catalog, Query query, String origin) {
      this.catalog = catalog;
      this.query = query;
      this.origin = origin;
   }
   
   @Override
   public ResultIterator<Record> execute() throws Exception {
      boolean disposed = closed.get();
      
      if(disposed) {
         throw new IllegalStateException("This statement has been closed");
      }
      String name = query.getTable();
      String expression = query.getSource();

      catalog.dropTable(origin, name);

      return new RecordIterator(Collections.EMPTY_LIST, expression);      
   }
}
