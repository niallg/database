package com.zuooh.database.imdb.statement;

import java.util.Collections;

import com.zuooh.database.Record;
import com.zuooh.database.RecordIterator;
import com.zuooh.database.ResultIterator;
import com.zuooh.database.Schema;
import com.zuooh.database.imdb.Catalog;
import com.zuooh.database.imdb.Table;
import com.zuooh.database.sql.Query;

public class CreateTableExecutor extends StatementExecutor {
   
   private final Catalog catalog;
   private final Query query;
   private final String origin;
   
   public CreateTableExecutor(Catalog catalog, Query query, String origin) {
      this.catalog = catalog;
      this.origin = origin;
      this.query = query;
   }
   
   @Override
   public ResultIterator<Record> execute() throws Exception {
      boolean disposed = closed.get();
      
      if(disposed) {
         throw new IllegalStateException("This statement has been closed");
      }
      String name = query.getTable();
      String expression = query.getSource();
      Table table = catalog.findTable(name);
      
      if(table == null) {
         Schema schema = query.getCreateSchema();
         
         if(schema == null) {
            throw new IllegalStateException("No schema dpecified for '" + expression + "'");
         }
         catalog.createTable(origin, name, schema);
      }
      return new RecordIterator(Collections.EMPTY_LIST, expression);      
   }
}
