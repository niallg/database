package com.zuooh.database.imdb.io.write;


public interface ChangeLog {
   void log(ChangeRecord record);
   void start();
   void stop();
}
