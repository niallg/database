package com.zuooh.database.imdb.predicate;

import com.zuooh.database.imdb.Cell;
import com.zuooh.database.imdb.Row;

public class LikePredicate extends Predicate {

   private final LikeEvaluator evaluator;
   private final Comparable right;
   private final String name;
   private final int index;
   
   public LikePredicate(Comparable right, String name, int index) {
      this.evaluator = new LikeEvaluator(right);
      this.right = right;
      this.index = index;
      this.name = name;
   }
   
   @Override
   public boolean accept(Row tuple) {
      Cell cell = tuple.getCell(index);
      Comparable left = cell.getValue();         
    
      if(left != null) {
         String source = String.valueOf(left);
      
         if(right != null) {
            return evaluator.like(source);
         }
      }
      return false;
   }
   
   @Override
   public String toString() {
      return String.format("%s like %s", name, right);
   } 
}
