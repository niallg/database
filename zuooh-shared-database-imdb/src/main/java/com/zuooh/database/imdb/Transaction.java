package com.zuooh.database.imdb;

public interface Transaction {
   String getName();
   String getOrigin();
   String getTable();
   String getToken();
   TransactionType getType();
   Long getSequence();
   Long getTime();
}
