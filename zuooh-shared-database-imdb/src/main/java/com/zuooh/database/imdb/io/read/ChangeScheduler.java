package com.zuooh.database.imdb.io.read;

public interface ChangeScheduler {
   void schedule(ChangeOperation operation);
}
