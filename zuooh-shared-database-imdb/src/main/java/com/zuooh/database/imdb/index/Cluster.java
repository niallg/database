package com.zuooh.database.imdb.index;

import com.zuooh.database.imdb.Row;

public interface Cluster extends Iterable<Row> {
   void insert(String key, Row tuple);
   void remove(String key);
   void clear();
   int size();
}
