package com.zuooh.database.imdb.export;

public interface ValueEscaper {
   String escape(String value);  
}
