package com.zuooh.database.imdb.io.write;

import java.io.IOException;

import com.zuooh.database.imdb.io.DataRecordCounter;
import com.zuooh.database.imdb.io.DataRecordWriter;

public interface ChangeRecordWriter {
   void write(DataRecordWriter writer, DataRecordCounter counter) throws IOException;
}
