package com.zuooh.database.standard;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;

import junit.framework.TestCase;

import com.almworks.sqlite4java.SQLite;
import com.zuooh.database.DatabaseConnection;
import com.zuooh.database.ResultIterator;
import com.zuooh.database.Statement;
import com.zuooh.database.bind.TableBinder;
import com.zuooh.database.bind.table.TableBuilder;
import com.zuooh.database.bind.table.attribute.AttributeTableBuilder;
import com.zuooh.database.bind.table.statement.SelectStatement;

public class RecordMapperTest extends TestCase  {
   
   private static class PersonProfile implements Serializable {
      int id;
      String name;
      int age;
      String address;
      double salary;
   }
   
   public void setUp() throws Exception {
      File file = new File("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib\\sqlite4java-win32-x86.dll");
      
      if(!file.exists()) {
         throw new FileNotFoundException("Unable to find library " + file);
      }
      SQLite.setLibraryPath("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");
      SQLite.loadLibrary();
   }

   public void testComplexTable() throws Exception {     
      StandardDatabase database = new StandardDatabase("c:\\work\\temp\\database", "C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");;
      DatabaseConnection connection = database.getConnection();
      
      try {
         connection.executeStatement("drop table if exists PersonProfile");
         connection.executeStatement("create table PersonProfile ("+ 
               "id int not null,"+
               "name text not null," +
               "age int not null," +
               "address text," +
               "salary double,"+
               "primary key(id))");
         
         Statement statement1 = connection.prepareStatement("insert into PersonProfile values (:id, :name, :age, :address, :salary)");
         
         statement1.set("id", 1);
         statement1.set("name", "Tom Harris");
         statement1.set("age", 22);
         statement1.set("address", "1 Some Street");
         statement1.set("salary", 100000);
         statement1.execute();
         
         Statement statement2 = connection.prepareStatement("insert into PersonProfile values (:id, :name, :age, :address, :salary)");
         
         statement2.set("id", 2);
         statement2.set("name", "Bob Hope");
         statement2.set("age", 32);
         statement2.set("address", "2 Some Street");
         statement2.set("salary", 140000);
         statement2.execute();
         
         Statement statement3 = connection.prepareStatement("insert into PersonProfile values (:id, :name, :age, :address, :salary)");
         
         statement3.set("id", 3);
         statement3.set("name", "Tony Mac");
         statement3.set("age", 25);
         statement3.set("address", "3 Some Street");
         statement3.set("salary", 110000);
         statement3.execute();
         
         Statement statement4 = connection.prepareStatement("insert into PersonProfile values (:id, :name, :age, :address, :salary)");
         
         statement4.set("id", 4);
         statement4.set("name", "Sally Anne");
         statement4.set("age", 42);
         statement4.set("address", "4 Some Street");
         statement4.set("salary", 130000);
         statement4.execute();
         
         Statement statement5 = connection.prepareStatement("insert into PersonProfile values (:id, :name, :age, :address, :salary)");
         
         statement5.set("id", 5);
         statement5.set("name", "Susie Chow");
         statement5.set("age", 24);
         statement5.set("address", "5 Some Street");
         statement5.set("salary", 140000);
         statement5.execute();
      } finally {
         connection.closeConnection();
      }
      TableBuilder scanner = new AttributeTableBuilder(database);
      TableBinder table = scanner.createTable("PersonProfile", PersonProfile.class, "id");
      SelectStatement builder = table.select();
      String text = builder.compile();
      
      assertEquals(text, "select id, name, age, address, salary from PersonProfile");
      
      ResultIterator<PersonProfile> iterator = builder.execute();
      
      while(iterator.hasMore()) {
         PersonProfile profile = iterator.next();         
         
         assertNotNull(profile);         
         System.err.printf("id='%s' name'=%s' age='%s' address='%s' salary='%s'%n", profile.id, profile.name, profile.age, profile.address, profile.salary);
      }
      
   }
}
