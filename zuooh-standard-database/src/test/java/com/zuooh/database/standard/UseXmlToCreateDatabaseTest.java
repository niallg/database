package com.zuooh.database.standard;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import junit.framework.TestCase;

import org.simpleframework.xml.core.Persister;

import com.almworks.sqlite4java.SQLite;
import com.zuooh.database.Database;
import com.zuooh.database.bind.load.TableDefinition;
import com.zuooh.database.bind.table.TableBuilder;
import com.zuooh.database.bind.table.attribute.AttributeTableBuilder;
import com.zuooh.database.build.dom.element.TableBinderElement;
import com.zuooh.database.build.dom.schema.TableBinderBuilder;

public class UseXmlToCreateDatabaseTest extends TestCase {
   
   private static final String SOURCE =
   "<table name=\"test\" type=\"com.zuooh.database.standard.UseXmlToCreateDatabaseTest$SomePerson\">\r\n" +
   "  <schema>\r\n"+
   "    <key>\r\n" +
   "      <column name=\"mail\" />\r\n" +
   "    </key>\r\n" +
   "    <column name=\"time\" type=\"date\" default=\"time\"/>\r\n" +
   "    <column name=\"jobTitle\" type=\"symbol\" />\r\n" +   
   "  </schema>\r\n" +
   "  <insert>\r\n" +
   "    <row>\r\n" +
   "      <value column=\"name\">John Doe</value>\r\n" +
   "      <value column=\"age\">22</value>\r\n" +
   "      <value column=\"addressNumber\">11</value>\r\n" +   
   "      <value column=\"addressHome\">Home</value>\r\n" +
   "      <value column=\"addressStreet\">Some Street</value>\r\n" +
   "      <value column=\"addressCity\">Sin City</value>\r\n" +
   "      <value column=\"addressCountry\">Atlantis</value>\r\n" +
   "      <value column=\"jobTitle\">Mayor</value>\r\n" +
   "      <value column=\"mail\">john.doe@yahoo.com</value>\r\n" +
   "    </row>\r\n" +
   "    <row>\r\n" +
   "      <value column=\"name\">Tom Waters</value>\r\n" +
   "      <value column=\"age\">32</value>\r\n" +
   "      <value column=\"addressNumber\">31</value>\r\n" +
   "      <value column=\"addressHome\">Apartment</value>\r\n" +
   "      <value column=\"addressStreet\">Blah Street</value>\r\n" +
   "      <value column=\"addressCity\">A City</value>\r\n" +
   "      <value column=\"addressCountry\">Ireland</value>\r\n" +
   "      <value column=\"jobTitle\">Software Engineer</value>\r\n" +
   "      <value column=\"mail\">tomw@gmail.com</value>\r\n" +
   "    </row>\r\n" +   
   "  </insert>\r\n" +
   "</table>\r\n";
         
   
   private static class SomeAddress implements Serializable {
      int number;
      String home;
      String street;
      String city;
      String country;
   }
   
   private static class SomePerson implements Serializable {
      String name;
      int age;
      SomeAddress address;
      String jobTitle;
      String mail;
   }
   
   public void setUp() throws Exception {
      File file = new File("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib\\sqlite4java-win32-x86.dll");
      
      if(!file.exists()) {
         throw new FileNotFoundException("Unable to find library " + file);
      }
      SQLite.setLibraryPath("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");
      SQLite.loadLibrary();
   }
   
   public void testDatabaseCreation() throws Exception {
      Persister persister = new Persister();
      TableBinderBuilder builder = persister.read(TableBinderBuilder.class, SOURCE);
      Database database = new StandardDatabase("c:\\work\\temp\\database", "C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");;
      TableBuilder binder = new AttributeTableBuilder(database);
      List<String> script = new LinkedList<String>();
      TableBinderElement tableElement = builder.createElement();
      TableDefinition tableDefinition = tableElement.createTable(binder);
      
      tableDefinition.createBinder(binder, script);      
      System.err.println(SOURCE);
      System.err.println();
   }

}
