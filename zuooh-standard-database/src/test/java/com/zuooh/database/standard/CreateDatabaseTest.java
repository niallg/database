package com.zuooh.database.standard;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedHashMap;
import java.util.Map;

import junit.framework.TestCase;

import com.almworks.sqlite4java.SQLite;
import com.zuooh.database.DatabaseConnection;
import com.zuooh.database.Record;
import com.zuooh.database.ResultIterator;
import com.zuooh.database.Statement;

public class CreateDatabaseTest extends TestCase {
   
   public void setUp() throws Exception {
      File file = new File("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib\\sqlite4java-win32-x86.dll");
      
      if(!file.exists()) {
         throw new FileNotFoundException("Unable to find library " + file);
      }
      SQLite.setLibraryPath("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");
      SQLite.loadLibrary();
   }
   
   public void testDotInColumnNames() throws Exception {
      StandardDatabase database = new StandardDatabase("c:\\work\\temp\\database", "C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");      
      DatabaseConnection connection = database.getConnection();
      
      try {
         connection.executeStatement("drop table if exists test");
         connection.executeStatement("create table test ("+ 
               "some_id int not null,"+
               "some_name text not null," +
               "some_age int not null," +
               "some_address text," +
               "some_salary double, "+
               "some_department symbol, "+               
               "primary key(some_id))");
      } finally {
         connection.closeConnection();
      }
   }

   public void testSimpleTable() throws Exception {     
      StandardDatabase database = new StandardDatabase("c:\\work\\temp\\database", "C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");
      DatabaseConnection connection = database.getConnection();
      
      try {
         connection.executeStatement("drop table if exists x");
         connection.executeStatement("create table x (x double not null, primary key(x))");
         Statement statement = connection.prepareStatement("insert into x values (:x)");
         
         statement.set("x", 1.2d);
         
         ResultIterator<Record> iterator = statement.execute();
         
         assertFalse(iterator.hasMore());
         assertTrue(iterator.isEmpty());      
         iterator.close();
         
         Statement select = connection.prepareStatement("select x from x where x > :x");
         
         select.set("x", 1.0d);
         
         ResultIterator<Record> results = select.execute();
         
         assertTrue(results.hasMore());
         assertFalse(results.isEmpty());      
         assertNotNull(results.next());
        
         Record result = results.next();
         
         assertEquals(result.getDouble("x"), 1.2d);
         assertFalse(results.hasMore());
      } finally {
         connection.closeConnection();
      }
   }
   
   public void testNamedParameters() throws Exception {
      StandardDatabase database = new StandardDatabase("c:\\work\\temp\\database", "C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");
      DatabaseConnection connection = database.getConnection();
      
      try {
         connection.executeStatement("drop table if exists profile");
         connection.executeStatement("create table profile ("+ 
               "id int not null,"+
               "name text not null," +
               "age int not null," +
               "address text," +
               "salary double,"+
               "category symbol,"+               
               "primary key(id))");
         Statement billHicks = connection.prepareStatement("insert into profile values (:theId, :theName, :theAge, :theStreet, :theSalary, :theCategory)");
         
         billHicks.set("theId", 1);
         billHicks.set("theName", "Bill Hicks");
         billHicks.set("theAge", 25);
         billHicks.set("theStreet", "Some Street");
         billHicks.set("theSalary", 110000);
         billHicks.set("theCategory", "Comedy");         
         billHicks.execute();
         
         Statement williamShatner = connection.prepareStatement("insert into profile values (:theId, :theName, :theAge, :theStreet, :theSalary, :theCategory)");
         
         williamShatner.set("theId", 2);
         williamShatner.set("theName", "William Shatner");
         williamShatner.set("theAge", 75);
         williamShatner.set("theStreet", "Star Trek");
         williamShatner.set("theSalary", 310000);
         williamShatner.set("theCategory", "Science Fiction");            
         williamShatner.execute();
         
         Statement select = connection.prepareStatement("select name, age, category from profile where salary > :x");
         
         select.set("x", 110000);
         
         ResultIterator<Record> iterator = select.execute();
         
         assertTrue(iterator.hasMore());
         assertTrue(iterator.hasMore());
         assertTrue(iterator.hasMore());
         assertFalse(iterator.isEmpty());
         
         Map<String, Integer> results = new LinkedHashMap<String, Integer>();
         
         while(iterator.hasMore()) {
            Record result = iterator.next();
            String name = result.getString("name");
            String category = result.getString("category");
            int age = result.getInteger("age");
            
            System.err.println("name="+name+" age="+age+" category="+category);
            results.put(name, age);
         }
         assertEquals(results.get("William Shatner"), new Integer(75));
      } finally {
         connection.closeConnection();
      }
   }
}
