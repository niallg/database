package com.zuooh.database.standard;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;

import junit.framework.TestCase;

import com.almworks.sqlite4java.SQLite;
import com.zuooh.database.DatabaseConnection;
import com.zuooh.database.Record;
import com.zuooh.database.ResultIterator;
import com.zuooh.database.Statement;
import com.zuooh.database.bind.TableBinder;
import com.zuooh.database.bind.table.TableBuilder;
import com.zuooh.database.bind.table.attribute.AttributeTableBuilder;
import com.zuooh.database.bind.table.statement.CreateStatement;
import com.zuooh.database.bind.table.statement.DropStatement;
import com.zuooh.database.bind.table.statement.InsertStatement;
import com.zuooh.database.bind.table.statement.SelectStatement;
import com.zuooh.database.bind.table.statement.UpdateStatement;

public class DropCreateInsertSelectUpdateTest extends TestCase {

   private static class NewPersonProfile implements Serializable {
      int id;
      String name;
      int age;
      String address;
      double salary;
      
      public NewPersonProfile(int id, String name, int age, String address, double salary) {
         this.id = id;
         this.name = name;
         this.age = age;
         this.address = address;
         this.salary = salary;
      }
   }
   
   public void setUp() throws Exception {
      File file = new File("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib\\sqlite4java-win32-x86.dll");
      
      if(!file.exists()) {
         throw new FileNotFoundException("Unable to find library " + file);
      }
      SQLite.setLibraryPath("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");
      SQLite.loadLibrary();
   }

   public void testComplexTable() throws Exception {     
      StandardDatabase database = new StandardDatabase("c:\\work\\temp\\database", "C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");;
      TableBuilder mapper = new AttributeTableBuilder(database);

      TableBinder table = mapper.createTable("NewPersonProfile", NewPersonProfile.class, "id");
      DropStatement dropStatement = table.dropIfExists();

      System.err.println(dropStatement.compile());
      System.err.println(dropStatement.execute());
      
      CreateStatement createStatement = table.create();
      
      System.err.println(createStatement.compile());
      System.err.println(createStatement.execute());
      
      InsertStatement insertStatement = table.insert();
      
      System.err.println(insertStatement.compile());
      
      System.err.println(insertStatement.execute(new NewPersonProfile(1, "Tom Harris", 22, "1 Some Street", 100000)));
      System.err.println(insertStatement.execute(new NewPersonProfile(2, "Bob Hope", 32, "2 Some Street", 140000)));
      System.err.println(insertStatement.execute(new NewPersonProfile(3, "Tony Mac", 25, "3 Some Street", 110000)));
      System.err.println(insertStatement.execute(new NewPersonProfile(4, "Sally Anne", 42, "4 Some Street", 130000)));
      System.err.println(insertStatement.execute(new NewPersonProfile(5, "Susie Chow", 24, "5 Some Street", 140000)));
      
      SelectStatement builder = table.select();      
      System.err.println(builder.compile());
      
      ResultIterator<NewPersonProfile> iterator = builder.execute();
      
      while(iterator.hasMore()) {
         NewPersonProfile profile = iterator.next();         
         
         assertNotNull(profile);         
         System.err.printf("id='%s' name'=%s' age='%s' address='%s' salary='%s'%n", profile.id, profile.name, profile.age, profile.address, profile.salary);
      }
      
      SelectStatement conditionalSelect = table.select();
      
      conditionalSelect.where("salary > 120000");
      System.err.println(conditionalSelect.compile());
      
      ResultIterator<NewPersonProfile> conditionalResults = conditionalSelect.execute();
      
      while(conditionalResults.hasMore()) {
         NewPersonProfile profile = conditionalResults.next();         
         
         assertNotNull(profile);         
         System.err.printf("(where salary > 120000) id='%s' name'=%s' age='%s' address='%s' salary='%s'%n", profile.id, profile.name, profile.age, profile.address, profile.salary);
      }
      UpdateStatement updateStatement = table.update();
      
      System.err.println(updateStatement.compile());      
      System.err.println(updateStatement.execute(new NewPersonProfile(1, "Tom Harris", 23, "1 Some Street", 140000)));
      
      SelectStatement specificSelect = table.select();
      
      specificSelect.where("id == 1");
      System.err.println(specificSelect.compile());
      
      ResultIterator<NewPersonProfile> specificResults = specificSelect.execute();
      
      while(specificResults.hasMore()) {
         NewPersonProfile profile = specificResults.next();         
         
         assertNotNull(profile);         
         System.err.printf("(where id == 1) id='%s' name'=%s' age='%s' address='%s' salary='%s'%n", profile.id, profile.name, profile.age, profile.address, profile.salary);
      }
      System.err.flush();      
      
      DatabaseConnection connection = database.getConnection();
      
      try {
         Statement statement = connection.prepareStatement("select name, sql from sqlite_master where type==\"table\"");
         ResultIterator<Record> records = statement.execute();
         
         while(records.hasMore()) {
            Record record = records.next();
            System.err.printf("name=%s, sql=%s%n", record.getString("name"), record.getString("sql"));
         }
      } finally {
         connection.closeConnection();
      }
      Thread.sleep(1000);
   }
}
