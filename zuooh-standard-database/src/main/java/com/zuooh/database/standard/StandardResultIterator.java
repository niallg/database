package com.zuooh.database.standard;

import static java.util.Collections.EMPTY_MAP;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import com.almworks.sqlite4java.SQLiteStatement;
import com.zuooh.database.Record;
import com.zuooh.database.RecordSchema;
import com.zuooh.database.ResultIterator;

public class StandardResultIterator implements ResultIterator {
   
   private final SQLiteStatement statement;
   private final AtomicInteger cursor;
   private final AtomicInteger count;
   private final String expression;
   private final RecordSchema schema;
   private final boolean empty;

   public StandardResultIterator(SQLiteStatement statement, RecordSchema schema, String expression) {
      this(statement, schema, expression, false);
   }
   
   public StandardResultIterator(SQLiteStatement statement, RecordSchema schema, String expression, boolean empty) {
      this.cursor = new AtomicInteger(1);
      this.count = new AtomicInteger();
      this.expression = expression;
      this.statement = statement;
      this.schema = schema;
      this.empty = empty;
   }

   @Override
   public synchronized Record next() throws Exception {
      if(empty) {
         throw new IllegalStateException("No results for statement '" + expression + "'");
      }
      int position = cursor.get();
      int current = count.get();
      
      if(position > current) {
         current = count.incrementAndGet(); // advance count
      }
      Set<String> names = schema.getColumns();
      int count = names.size();
      
      if(!names.isEmpty()) {
         Map<String, Object> record = new LinkedHashMap<String, Object>();
         List<String> columns = new ArrayList<String>(names); // dodgy?
         
         for(int i = 0; i < count; i++) {
            Object value = statement.columnValue(i);
            String column = columns.get(i);
            
            if(value != null) {
               record.put(column, value);
            }
         }
         return new StandardRecord(record, schema);
      }
      return new StandardRecord(EMPTY_MAP, schema);    
   }
   
   @Override
   public synchronized Record fetchFirst() throws Exception{
      List<Record> records = fetchNext(1);
      
      if(!records.isEmpty()) {
         return records.get(0);
      }
      return null;
   }
   
   @Override
   public synchronized Record fetchLast() throws Exception{
      List<Record> records = fetchAll();
      int length = records.size();
      
      if(length > 0) {         
         return records.get(length - 1);
      }
      return null;
   }
   
   @Override
   public synchronized List<Record> fetchAll() throws Exception{
      return fetchNext(Integer.MAX_VALUE);
   }
   
   @Override
   public synchronized List<Record> fetchNext(int count) throws Exception{
      List<Record> list = new LinkedList<Record>();
      
      for(int i = 0; i < count; i++) {
         if(!hasMore()) {
            return list;
         }
         Record value = next();
         
         if(value != null) {
            list.add(value);
         } 
      }
      return list;
   }   

   @Override
   public synchronized boolean hasMore() {  
      try {
         if(!empty) {
            int position = cursor.get();
            int current = count.get();
            
            if(position > current) {
               return true;
            }
            if(statement.step()) {
               cursor.getAndIncrement(); // advance cursor
               return true;
            }
         }
      } catch(Exception e) {
         throw new IllegalStateException("Problem stepping through '" + expression + "'", e);
      }
      return false;
   }
   
   @Override
   public synchronized boolean isEmpty() {
      return empty;
   }
   
   @Override
   public synchronized void close() {
      statement.dispose();
   }

}
