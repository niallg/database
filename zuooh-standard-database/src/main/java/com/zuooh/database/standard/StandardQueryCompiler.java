package com.zuooh.database.standard;

import static com.zuooh.database.sql.Verb.CREATE_TABLE;

import java.util.Collections;
import java.util.Map;

import com.zuooh.database.sql.Query;
import com.zuooh.database.sql.Verb;
import com.zuooh.database.sql.compile.QueryCompiler;

public class StandardQueryCompiler extends QueryCompiler {
   
   protected Map<String, String> translations;
   
   public StandardQueryCompiler() {
      this(Collections.EMPTY_MAP);
   }
   
   public StandardQueryCompiler(Map<String, String> translations) {
      this.translations = translations;
   }

   protected QueryCompiler resolve(Query query) {
      Verb verb = query.getVerb();
      
      if(verb == CREATE_TABLE) {
         return new StandardCreateTableCompiler();
      }      
      return super.resolve(query);
   }
}
