package com.zuooh.database.standard;

import java.io.File;
import java.util.concurrent.atomic.AtomicReference;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import com.zuooh.common.FileSystem;
import com.zuooh.common.FileSystemDirectory;
import com.zuooh.database.Database;
import com.zuooh.database.bind.DatabaseBinder;
import com.zuooh.database.bind.load.DatabaseDefinition;
import com.zuooh.database.build.dom.DatabaseContext;
import com.zuooh.database.build.dom.element.DatabaseBinderElement;
import com.zuooh.database.build.dom.schema.DatabaseBinderBuilder;

public class StandardDatabaseLoader {
   
   private final AtomicReference<DatabaseBinder> reference;
   private final DatabaseContextDelegate delegate;  
   private final File source; 
   private final File work;
   
   public StandardDatabaseLoader(Database database, String path, String source, String work) {
      this.delegate = new DatabaseContextDelegate(database, path, work);
      this.reference = new AtomicReference<DatabaseBinder>();
      this.work = new File(path, work);
      this.source = new File(source);
   }
   
   public synchronized DatabaseBinder createBinder() throws Exception {
      DatabaseBinder binder = reference.get();
      
      if(binder == null) {
         Database database = delegate.getDatabase();
         Serializer serializer = delegate.getSerializer();
        
         if(!work.exists()) {
            work.mkdirs();
         }
         DatabaseBinderBuilder builder = serializer.read(DatabaseBinderBuilder.class, source);
         DatabaseBinderElement element = builder.createElement();
         DatabaseDefinition definition = element.createDatabase(delegate);
      
         binder = definition.createDatabase(database);      
         reference.set(binder);
      }
      return binder;
   }
   
   private static class DatabaseContextDelegate implements DatabaseContext {

      private final FileSystem fileSystem;
      private final Persister serializer;
      private final Database database; 
      
      public DatabaseContextDelegate(Database database, String path, String work) {      
         this.fileSystem = new FileSystemDirectory(path, work, false);           
         this.serializer = new Persister();
         this.database = database;
      }

      @Override
      public FileSystem getFileSystem() {
         return fileSystem;
      }

      @Override
      public Serializer getSerializer() {
         return serializer;
      }

      @Override
      public Database getDatabase() {        
         return database;
      }
   }
}
