package com.zuooh.database.standard;

import static com.zuooh.database.data.DataType.DOUBLE;
import static com.zuooh.database.data.DataType.INT;
import static com.zuooh.database.data.DataType.LONG;
import static com.zuooh.database.data.DataType.SYMBOL;
import static com.zuooh.database.data.DataType.TEXT;
import static com.zuooh.database.sql.ParameterType.NAME;
import static com.zuooh.database.sql.ParameterType.VALUE;

import java.util.ArrayList;
import java.util.List;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteStatement;
import com.zuooh.database.Record;
import com.zuooh.database.RecordIterator;
import com.zuooh.database.RecordSchema;
import com.zuooh.database.ResultCache;
import com.zuooh.database.ResultIterator;
import com.zuooh.database.data.DataType;
import com.zuooh.database.sql.Condition;
import com.zuooh.database.sql.Parameter;
import com.zuooh.database.sql.ParameterType;
import com.zuooh.database.sql.Query;
import com.zuooh.database.sql.Verb;
import com.zuooh.database.sql.WhereClause;

public class StandardStatement extends StandardStatementBuilder {
   
   private final StandardStatementTracer tracer;
   private final SQLiteConnection connection;
   private final RecordSchema schema;
   private final ResultCache cache;
   private final Query query;
   
   public StandardStatement(StandardStatementTracer tracer, SQLiteConnection connection, ResultCache cache, Query query) {
      this.schema = new RecordSchema();
      this.connection = connection;
      this.tracer = tracer;
      this.cache = cache;
      this.query = query;
   }
   
   @Override
   public synchronized ResultIterator<Record> execute() throws Exception {
      boolean disposed = closed.get();
      
      if(disposed) {
         throw new IllegalStateException("This statement has been closed");
      }
      WhereClause clause = query.getWhereClause();
      List<Condition> conditions = clause.getConditions();
      List<Parameter> parameters = query.getParameters();
      int conditionCount = conditions.size();
      int parameterCount = parameters.size();
      int totalCount = parameterCount + conditionCount;
      
      if(totalCount > 0) {
         Comparable[] list = new Comparable[totalCount];
         
         for(int i = 0; i < parameterCount; i++) {
            Parameter parameter = parameters.get(i);
            String name = parameter.getName();
            ParameterType type = parameter.getType();
            
            if(type == VALUE) {
               list[i] = parameter.getValue();
            } else if(type == NAME) {
               list[i] = attributes.get(name);  
            } else {
               throw new IllegalStateException("Parameter '" + parameter + "' is not a named or literal parameter");            
            }        
         }
         for(int i = 0; i < conditionCount; i++) {
            Condition condition = conditions.get(i);
            Parameter parameter = condition.getParameter();
            String name = parameter.getName();
            ParameterType type = parameter.getType();
            
            if(type == VALUE) {
               list[i + parameterCount] = parameter.getValue();
            } else if(type == NAME) {
               list[i + parameterCount] = attributes.get(name);  
            } else {
               throw new IllegalStateException("Parameter '" + parameter + "' is not a named or literal parameter");            
            } 
         }
         return execute(list);
      }
      return execute(EMPTY);
   }  
   
   protected synchronized ResultIterator<Record> execute(Comparable[] values) throws Exception {    
      String table = query.getTable();
      Verb verb = query.getVerb();
      String expression = compiler.compile(query, values);
      
      if(verb == Verb.SELECT) {
         List<Record> results = cache.check(table, expression);
         
         if(results != null) {
            return new RecordIterator(results, expression); 
         }         
      }    
      long start = System.currentTimeMillis();
      
      try {
         SQLiteStatement statement = connection.prepare(expression, true);  
         
         try {
            List<Record> results = execute(values, statement);
               
            if(verb == Verb.SELECT) {
               cache.update(table, expression, results);        
            } else {
               cache.clear(table); // insert, update, delete modifies the database
            }
            return new RecordIterator(results, expression);
         } finally {
            if(!statement.isDisposed()) {
               statement.dispose();
            }
         }
      } finally {
         long finish = System.currentTimeMillis();
         long duration = finish - start;
         
         tracer.traceStatement(expression, duration);
      }
   }
   
   protected synchronized List<Record> execute(Comparable[] values, SQLiteStatement statement) throws Exception {
      String expression = statement.toString();
      int columns = statement.columnCount();
      
      for(int i = 0; i < columns; i++) {
         String name = statement.getColumnName(i);
         
         if(name == null) {
            throw new IllegalStateException("Could not determine schema for '" + expression + "'");
         }
         schema.addColumn(name);
      } 
      int require = statement.getBindParameterCount();
      int count = values.length;
      
      if(require > count) {
         throw new IllegalStateException("Require " + require + " parameters but only got " + count);
      }
      for(int i = 0; i < require; i++) { // SQLite does a weird thing with repeating named parameters e.g "update x set i = :i where i = :i" -> 1 param
         Comparable value = values[i];
         
         if(value != null) {          
            Class type = value.getClass();
            DataType convert = DataType.resolveType(type);
            Comparable data = converter.convert(convert, value);
            
            if(convert == INT) {
               statement.bind(i + 1, (Integer)data);
            } else if(convert == LONG) {
               statement.bind(i + 1, (Long)data);               
            } else if(convert == DOUBLE) {
               statement.bind(i + 1, (Double)data);
            } else if(convert == TEXT) {
               statement.bind(i + 1, (String)data);
            } else if(convert == SYMBOL) {
               statement.bind(i + 1, (String)data);               
            } else {
               throw new IllegalStateException("Unknown type " + type + " from " + expression + " at " + i);
            }
         } else {
            statement.bindNull(i + 1);
         }
      }  
      List<Record> records = new ArrayList<Record>();   
      
      if(statement.step()) {
         ResultIterator<Record> iterator = new StandardResultIterator(statement, schema, expression);
         
         while(iterator.hasMore()) {
            Record record = iterator.next();
            
            if(record != null) {
               records.add(record);
            }
         }    
      }
      return records;
   }  
}
