package com.zuooh.database.standard;

import java.io.File;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.almworks.sqlite4java.SQLite;

public class StandardLibraryLoader {
   
   private final AtomicBoolean loaded;   
   private final File library;
   private final boolean debug;
   
   public StandardLibraryLoader(String library) {
      this(library, false);
   }
   
   public StandardLibraryLoader(String library, boolean debug) {
      this.loaded = new AtomicBoolean(false);
      this.library = new File(library);     
      this.debug = debug;
   }    
   
   public void ensureLibraryLoaded() throws Exception {
      if(!library.exists()) {
         throw new IllegalStateException("Library path '" + library + "' does not exist");
      }
      if(!loaded.get()) {
         Logger logger = Logger.getLogger("com.almworks.sqlite4java");
         String location = library.getAbsolutePath();
         
         if(!debug) {
            logger.setLevel(Level.OFF);
         }
         SQLite.setLibraryPath(location);
         SQLite.loadLibrary(); 
         loaded.set(true);
      }
   }
}
