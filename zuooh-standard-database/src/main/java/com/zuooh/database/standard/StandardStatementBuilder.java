package com.zuooh.database.standard;

import com.zuooh.database.StatementTemplate;
import com.zuooh.database.data.DataConverter;
import com.zuooh.database.sql.compile.QueryCompiler;

public abstract class StandardStatementBuilder extends StatementTemplate {
   
   protected static final Comparable[] EMPTY = {};
   
   protected final DataConverter converter;
   protected final QueryCompiler compiler;
   
   protected StandardStatementBuilder() {
      this.compiler = new StandardQueryCompiler();
      this.converter = new DataConverter();
   }
}