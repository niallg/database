package com.zuooh.database.standard;

import java.io.File;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import com.almworks.sqlite4java.SQLiteConnection;
import com.zuooh.common.util.StackTrace;
import com.zuooh.database.Database;
import com.zuooh.database.DatabaseConnection;
import com.zuooh.database.ResultCache;
import com.zuooh.database.sql.QueryConverter;
import com.zuooh.database.sql.build.QueryProcessor;

public class StandardDatabase implements Database {
   
   private final AtomicReference<String> caller;
   private final StandardStatementTracer tracer;
   private final StandardLibraryLoader loader;
   private final QueryConverter converter;
   private final QueryProcessor processor;
   private final Semaphore semaphore;
   private final ResultCache cache;
   private final File directory;
   
   public StandardDatabase(String path, String library) {
      this(path, library, false);
   }
   
   public StandardDatabase(String path, String library, boolean debug) {
      this.loader = new StandardLibraryLoader(library);
      this.caller = new AtomicReference<String>();
      this.converter = new StandardStatementConverter();
      this.tracer = new StandardStatementTracer(debug);
      this.processor = new QueryProcessor(converter);
      this.cache = new ResultCache();
      this.semaphore = new Semaphore(1);
      this.directory = new File(path);      
   }
   
   @Override
   public synchronized DatabaseConnection getConnection() throws Exception {
      String previous = caller.get();
      String current = StackTrace.generateStackTrace();
      
      if(!semaphore.tryAcquire(1,20000, TimeUnit.MILLISECONDS)) {
         throw new IllegalStateException("Could not acquire connection after 20 seconds as it is used by: " + previous);
      }     
      SQLiteConnection connection = new SQLiteConnection(directory);
      
      loader.ensureLibraryLoaded();
      connection.open(true);
      caller.set(current);
      
      return new StandardDatabaseConnection(tracer, connection, cache, processor, semaphore);    
   }
}
