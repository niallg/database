package com.zuooh.database.standard;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

import com.almworks.sqlite4java.SQLiteConnection;
import com.zuooh.database.DatabaseConnection;
import com.zuooh.database.ResultCache;
import com.zuooh.database.Statement;
import com.zuooh.database.sql.Query;
import com.zuooh.database.sql.build.QueryProcessor;

public class StandardDatabaseConnection implements DatabaseConnection {
   
   private final StandardStatementTracer tracer;
   private final SQLiteConnection connection;
   private final QueryProcessor processor;
   private final AtomicBoolean disposed;
   private final Semaphore semaphore;
   private final ResultCache cache;
   
   public StandardDatabaseConnection(StandardStatementTracer tracer, SQLiteConnection connection, ResultCache cache, QueryProcessor processor, Semaphore semaphore) {
      this.disposed = new AtomicBoolean();
      this.connection = connection;
      this.processor = processor;
      this.semaphore = semaphore;
      this.tracer = tracer;
      this.cache = cache;
   } 

   @Override
   public synchronized Statement prepareStatement(String expression) throws Exception {
      if(disposed.get()) {
         throw new IllegalStateException("Unable to execute '" + expression + "' as database connection was explicitly closed");
      }
      return prepareStatement(expression, true);
   }
   
   @Override
   public synchronized Statement prepareStatement(String expression, boolean flag) throws Exception {
      if(disposed.get()) {
         throw new IllegalStateException("Unable to execute '" + expression + "' as database connection was explicitly closed");
      }
      if(!connection.isOpen()) {
         throw new IllegalStateException("Unable to execute '" + expression + "' as database connection is not open");
      }
      Query query = (Query)processor.process(expression); // just for validation
       
      if(query == null) {
         throw new IllegalStateException("Unable to create quert from '" + expression + "'");
      }
      return new StandardStatement(tracer, connection, cache, query);
   }
   
   @Override
   public synchronized void executeStatement(String expression) throws Exception {    
      Statement statement = prepareStatement(expression);
      
      if(statement != null) {
         statement.execute();
      }
   }
   
   @Override
   public synchronized void closeConnection() throws Exception {
      if(disposed.get()) {
         throw new IllegalStateException("Database connection has alreadt been closed");
      }
      disposed.set(true);
      connection.dispose();
      semaphore.release(1);
   }
}
