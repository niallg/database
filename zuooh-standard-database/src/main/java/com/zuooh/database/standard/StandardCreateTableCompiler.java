package com.zuooh.database.standard;

import static com.zuooh.database.data.DataConstraint.KEY;
import static com.zuooh.database.data.DataConstraint.REQUIRED;
import static com.zuooh.database.data.DataType.BOOLEAN;
import static com.zuooh.database.data.DataType.BYTE;
import static com.zuooh.database.data.DataType.CHAR;
import static com.zuooh.database.data.DataType.DATE;
import static com.zuooh.database.data.DataType.DOUBLE;
import static com.zuooh.database.data.DataType.FLOAT;
import static com.zuooh.database.data.DataType.INT;
import static com.zuooh.database.data.DataType.LONG;
import static com.zuooh.database.data.DataType.SHORT;
import static com.zuooh.database.data.DataType.SYMBOL;
import static com.zuooh.database.data.DataType.TEXT;
import static com.zuooh.database.function.DefaultFunction.CURRENT_TIME;
import static com.zuooh.database.function.DefaultFunction.LITERAL;
import static com.zuooh.database.function.DefaultFunction.SEQUENCE;

import java.util.Collections;
import java.util.Map;

import com.zuooh.database.Column;
import com.zuooh.database.data.DataConstraint;
import com.zuooh.database.data.DataType;
import com.zuooh.database.function.DefaultFunction;
import com.zuooh.database.function.DefaultValue;
import com.zuooh.database.sql.compile.CreateTableCompiler;

public class StandardCreateTableCompiler extends CreateTableCompiler {
   
   public StandardCreateTableCompiler() {
      this(Collections.EMPTY_MAP);
   }
   
   public StandardCreateTableCompiler(Map<String, String> translations) {
      this.translations = translations;
   } 

   @Override
   public String compileType(Column column) throws Exception {
      DataType type = column.getDataType();
      String name = type.getName();
      
      if(type == INT) {
         return "integer";
      }
      if(type == LONG) {
         return "integer";
      }
      if(type == SHORT) {
         return "integer";
      }
      if(type == BYTE) {
         return "integer";
      }
      if(type == FLOAT) {
         return "real";
      }
      if(type == DOUBLE) {
         return "real";
      }
      if(type == TEXT) {
         return "text";
      }
      if(type == SYMBOL) {
         return "text";
      }      
      if(type == CHAR) {
         return "text";
      }
      if(type == BOOLEAN) {
         return "text";
      }
      if(type == DATE) {
         return "integer";
      }        
      return name;     
   }
   
   @Override
   public String compileConstraint(Column column) throws Exception {
      DataConstraint constraint = column.getDataConstraint();
      
      if(constraint == REQUIRED) {
         return "not null";
      }
      if(constraint == KEY) {
         return "primary key";
      }
      return null;
   }
   
   @Override
   public String compileDefault(Column column) throws Exception {
      DefaultValue value = column.getDefaultValue();
      DefaultFunction function = value.getFunction();
      String expression = value.getExpression();
      
      if(function == SEQUENCE) {
         return null;
      }
      if(function == CURRENT_TIME) {
         return "default current_timestamp";
      }
      if(function == LITERAL) {
         return "default " + expression;
      }
      return null;
   }
}
