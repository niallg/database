package com.zuooh.database.standard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StandardStatementTracer {
   
   private static final Logger LOG = LoggerFactory.getLogger(StandardStatementTracer.class);   
   
   private final boolean enabled;
   
   public StandardStatementTracer(){
      this(false);
   }
   
   public StandardStatementTracer(boolean enabled) {
      this.enabled = enabled;
   }

   public void traceStatement(String statement, long duration) {
      if(enabled) {
         LOG.info("SQL [" + statement + "] took " + duration + " milliseconds");
      }
   }
}
