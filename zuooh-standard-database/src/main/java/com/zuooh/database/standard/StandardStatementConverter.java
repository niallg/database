package com.zuooh.database.standard;

import com.zuooh.database.sql.Query;
import com.zuooh.database.sql.QueryConverter;

public class StandardStatementConverter implements QueryConverter<Query> {    
   
   @Override
   public Query convert(Query query) {
      return query;
   }
}
