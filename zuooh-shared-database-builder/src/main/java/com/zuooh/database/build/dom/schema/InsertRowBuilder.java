package com.zuooh.database.build.dom.schema;

import java.util.LinkedList;
import java.util.List;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import com.zuooh.database.build.dom.element.InsertRowElement;
import com.zuooh.database.build.dom.element.ValueElement;

@Root
public class InsertRowBuilder {
   
   @ElementList(entry="value", inline=true)
   private List<ValueBuilder> values;
   
   public InsertRowBuilder() {
      this.values = new LinkedList<ValueBuilder>();
   }
   
   public InsertRowElement createElement() {
      InsertRowElement element = new InsertRowElement();
      
      if(values != null) {
         for(ValueBuilder builder : values) {
            ValueElement value = builder.createElement();
            element.addValue(value);
         }
      }
      return element;
   }
}
