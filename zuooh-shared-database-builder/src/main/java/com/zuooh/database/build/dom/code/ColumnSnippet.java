package com.zuooh.database.build.dom.code;

import java.util.Map;

import com.zuooh.code.CodeBuilder;
import com.zuooh.code.load.CodeSnippet;
import com.zuooh.database.Column;
import com.zuooh.database.build.dom.schema.ColumnBuilder;
import com.zuooh.database.data.DataConstraint;
import com.zuooh.database.data.DataType;

public class ColumnSnippet implements CodeSnippet {

   private final DataConstraint constraint;
   private final ColumnBuilder builder;
   private final String expression;
   private final DataType data;
   private final String name;

   public ColumnSnippet(ColumnBuilder builder, DataConstraint constraint, DataType data, String expression, String name) {
      this.constraint = constraint;
      this.expression = expression;
      this.builder = builder;
      this.name = name;
      this.data = data; 
   }    

   @Override
   public String render(CodeBuilder builder, Map<String, String> state) throws Exception {
      String variableId = getVariableId();
      String constraintName = constraint.name();    
      String dataName = data.name();  
      
      builder.createConstant(variableId, Column.class, 
            new Class[]{DataConstraint.class, DataType.class, String.class, String.class, String.class}, 
            new String[]{constraintName, dataName, expression, name, name});
      
      return variableId;
   }

   public String getVariableId() {
      int uniqueCode = System.identityHashCode(builder); 
      String text = String.format("column_%s_%s", name, uniqueCode);
      
      return text.toUpperCase();
   }
}
