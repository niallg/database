package com.zuooh.database.build.dom.element;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.code.load.CodeContext;
import com.zuooh.code.load.CodeVisitor;
import com.zuooh.common.FileSystem;
import com.zuooh.database.Database;
import com.zuooh.database.DatabaseConnection;
import com.zuooh.database.bind.TableBinder;
import com.zuooh.database.bind.load.DatabaseDefinition;
import com.zuooh.database.bind.load.TableDefinition;
import com.zuooh.database.bind.table.TableBuilder;
import com.zuooh.database.bind.table.attribute.AttributeTableBuilder;
import com.zuooh.database.bind.table.statement.CreateStatement;
import com.zuooh.database.build.dom.DatabaseContext;
import com.zuooh.database.build.dom.code.DatabaseBinderSnippet;
import com.zuooh.database.build.dom.schema.DatabaseBinderBuilder;

public class DatabaseBinderElement {
   
   private static final Logger LOG = LoggerFactory.getLogger(DatabaseBinderElement.class);
   
   private final List<IncludeTableElement> includes;
   private final DatabaseBinderBuilder builder;
   private final String details;   
   private final String name;
   private final int version;  
   
   public DatabaseBinderElement(DatabaseBinderBuilder builder, String details, String name, int version) {
      this.includes = new LinkedList<IncludeTableElement>();
      this.details = details;
      this.builder = builder;
      this.version = version;
      this.name = name;
   }
   
   public void addInclude(IncludeTableElement include) {
      includes.add(include);
   }   
   
   public DatabaseDefinition createDatabase(DatabaseContext context) throws Exception {
      Database database = context.getDatabase();
      FileSystem fileSystem = context.getFileSystem();
      DatabaseDefinition databaseElement = new DatabaseDefinition(name, version);
      Map<String, TableBinderElement> elements = new HashMap<String, TableBinderElement>();
      Map<String, TableBinder> schema = new HashMap<String, TableBinder>();
      TableBuilder binder = new AttributeTableBuilder(database, version);
      List<String> script = new LinkedList<String>();
      Properties properties = new Properties();
      
      try {
         InputStream source = fileSystem.openExternalFile(details);
         
         properties.load(source);
         source.close();
      }catch(Exception e) {
         LOG.info("Could not load database properties '" + details + "'");
      }
      for(IncludeTableElement include : includes) {
         TableBinderElement tableElement = include.createElement(context);
         TableDefinition tableDefinition = tableElement.createTable(binder);
         TableBinder tableBinder = tableDefinition.createBinder(binder, script);
         String name = tableDefinition.getName();
         String file = include.getFile();

         if(schema.containsKey(name)) {
            throw new IllegalStateException("Table of name '" + name + "' has already been declared");
         }
         CreateStatement createStatement = tableBinder.createIfNotExists();
         String createExpression = createStatement.compile();
         
         databaseElement.addResource(file);
         databaseElement.addTable(tableDefinition);     
         
         if(!properties.containsKey(name)) {
            databaseElement.addStatement(createExpression);    
            
            try {
               createStatement.execute();
            } catch(Exception e) {
               LOG.info("Error executing create statement '" + createExpression + "'", e);
            }
            for(String statement : script) {
               DatabaseConnection connection = database.getConnection();
               
               try {
                  connection.executeStatement(statement);
                  databaseElement.addStatement(statement);
               } catch(Exception cause) {
                  String message = cause.getMessage();
                  Class type = cause.getClass();
                  String exception = type.getSimpleName();
                  
                  LOG.info("Error of type '" + exception + "' occurred executing '" + statement + "', reason was '" + message + "'");
               } finally {
                  connection.closeConnection();
               }
            }
         }         
         properties.setProperty(name, file);
         elements.put(name, tableElement);
         schema.put(name, tableBinder);
         script.clear();         
      }   
      try {
         OutputStream result = fileSystem.createExternalFile(details);
         
         properties.store(result, "");
         result.close();
      }catch(Exception e) {
         LOG.info("Could not save database properties '" + details + "'");
      }
      return databaseElement;
   }
   
   public String generateCode(CodeContext application, CodeVisitor visitor) throws Exception {
      DatabaseBinderSnippet snippet = new DatabaseBinderSnippet(builder, name, version);
      TableBuilder binder = new AttributeTableBuilder(null, version);
      List<String> script = new LinkedList<String>();
      
      for(IncludeTableElement include : includes) {
         TableBinderElement tableElement = include.createElement(application);         
         TableDefinition tableDefinition = tableElement.createTable(binder);
         TableBinder tableBinder = tableDefinition.createBinder(binder, script);
         String tableId = tableElement.generateTable(binder, application, visitor);
         CreateStatement createStatement = tableBinder.createIfNotExists();
         String createExpression = createStatement.compile();
         String file = include.getFile();
         
         snippet.addResource(file);
         snippet.addStatement(createExpression);
         
         for(String statement : script) {
            snippet.addStatement(statement);
         } 
         snippet.addTable(tableId);
         script.clear();
      }
      visitor.visitSnippet(snippet);
      return snippet.getVariableId();
   }
}
