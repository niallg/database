package com.zuooh.database.build.dom.schema;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

import com.zuooh.database.build.dom.element.ValueElement;

@Root
public class ValueBuilder {

   @Attribute
   private String column;

   @Text
   private String value;
   
   public ValueElement createElement() {
      return new ValueElement(column, value);
   }
}
