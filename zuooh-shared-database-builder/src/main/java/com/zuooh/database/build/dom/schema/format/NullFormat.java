package com.zuooh.database.build.dom.schema.format;

public class NullFormat extends ValueFormat {

   @Override
   public String formatValue(String value) {
      return "NULL";
   }      
}
