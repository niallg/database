package com.zuooh.database.build.dom.schema;

import java.util.LinkedList;
import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import com.zuooh.code.load.CodeContext;
import com.zuooh.code.load.CodeProcessor;
import com.zuooh.code.load.CodeVisitor;
import com.zuooh.database.build.dom.element.DatabaseBinderElement;
import com.zuooh.database.build.dom.element.IncludeTableElement;

@Root
public class DatabaseBinderBuilder implements CodeProcessor {   
   
   @ElementList(entry="include", inline=true)
   private List<IncludeTableBuilder> includes;
   
   @Attribute 
   private String properties;
   
   @Attribute
   private String name;
   
   @Attribute(required=false)
   private int version;     

   public DatabaseBinderBuilder() {
      this.includes = new LinkedList<IncludeTableBuilder>();
   }
   
   public DatabaseBinderElement createElement() throws Exception {
      DatabaseBinderElement element = new DatabaseBinderElement(this, properties, name, version);
      
      for(IncludeTableBuilder include : includes) {
         IncludeTableElement result = include.createElement();
         element.addInclude(result);
      }
      return element;
   }

   @Override
   public String process(CodeContext application, CodeVisitor visitor) throws Exception {
      DatabaseBinderElement element = createElement();
      return element.generateCode(application, visitor);
   }   
  
}
