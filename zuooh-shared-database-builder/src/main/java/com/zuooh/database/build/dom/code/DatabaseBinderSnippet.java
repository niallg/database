package com.zuooh.database.build.dom.code;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.zuooh.code.CodeBuilder;
import com.zuooh.code.load.CodeSnippet;
import com.zuooh.database.bind.load.DatabaseDefinition;
import com.zuooh.database.bind.load.TableDefinition;
import com.zuooh.database.build.dom.schema.DatabaseBinderBuilder;

public class DatabaseBinderSnippet implements CodeSnippet {

   private final DatabaseBinderBuilder builder;
   private final List<String> statements;
   private final List<String> tables;
   private final List<String> files;
   private final String name;
   private final int version;

   public DatabaseBinderSnippet(DatabaseBinderBuilder builder, String name, int version) {
      this.statements = new LinkedList<String>();
      this.tables = new LinkedList<String>();
      this.files = new LinkedList<String>();          
      this.builder = builder;
      this.version = version;
      this.name = name;    
   }
   
   public void addResource(String file) {
      files.add(file);
   }
   
   public void addStatement(String statement) {
      statements.add(statement);
   }

   public void addTable(String tableId) {
      tables.add(tableId);
   }

   @Override
   public String render(CodeBuilder builder, Map<String, String> state) throws Exception {
      String variableId = getVariableId();

      builder.callConstructor(DatabaseDefinition.class, variableId, 
            new Class[]{String.class, int.class}, 
            new String[]{name, String.valueOf(version)});
      
      for(String tableId : tables) {
         builder.callMethod(DatabaseDefinition.class, variableId, "addTable", TableDefinition.class, tableId);
      }
      for(String statement : statements) {
         builder.callMethod(DatabaseDefinition.class, variableId, "addStatement", String.class, statement);
      }
      for(String file : files) {
         builder.addSourceFile(file);
      }
      return variableId;
   }

   public String getVariableId() {
      return "database" + System.identityHashCode(builder);
   }
}
