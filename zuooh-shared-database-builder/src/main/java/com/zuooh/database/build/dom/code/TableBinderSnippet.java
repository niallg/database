package com.zuooh.database.build.dom.code;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.zuooh.code.CodeBuilder;
import com.zuooh.code.load.CodeSnippet;
import com.zuooh.database.Column;
import com.zuooh.database.bind.load.TableDefinition;
import com.zuooh.database.build.dom.schema.TableBinderBuilder;

public class TableBinderSnippet implements CodeSnippet {

   private final TableBinderBuilder builder;
   private final List<String> columns;
   private final List<String> keys;
   private final String name;
   private final Class type;

   public TableBinderSnippet(TableBinderBuilder builder, String name, Class type) {
      this.columns = new LinkedList<String>();
      this.keys = new LinkedList<String>();
      this.builder = builder;
      this.name = name;    
      this.type = type;
   }    

   public void addKey(String keyId) {
      keys.add(keyId);
   }
   
   public void addColumn(String columnId) {
      columns.add(columnId);
   }

   @Override
   public String render(CodeBuilder builder, Map<String, String> state) throws Exception {
      String variableId = getVariableId();
      String typeName = type.getName();

      builder.callConstructor(TableDefinition.class, variableId, new Class[]{String.class, Class.class}, new String[]{name, typeName});
      
      for(String keyId : keys) {
         builder.callMethod(TableDefinition.class, variableId, "addKey", Column.class, keyId);
      }
      for(String columnId : columns) {
         builder.callMethod(TableDefinition.class, variableId, "addColumn", Column.class, columnId);
      }
      return variableId;
   }

   public String getVariableId() {
      return "table" + System.identityHashCode(builder);
   }
}
