package com.zuooh.database.build.dom.schema;

import java.util.LinkedList;
import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import com.zuooh.database.build.dom.element.ColumnElement;
import com.zuooh.database.build.dom.element.InsertRowElement;
import com.zuooh.database.build.dom.element.TableBinderElement;

@Root
public class TableBinderBuilder {

   @Attribute
   private String name;

   @Attribute
   private Class type;

   @Path("schema")
   @ElementList(entry = "column")
   private List<ColumnBuilder> key;

   @Path("schema")
   @ElementList(entry = "column", inline = true, required = false)
   private List<ColumnBuilder> columns;

   @Path("insert")
   @ElementList(entry = "row", inline = true, required = false)
   private List<InsertRowBuilder> inserts;

   public TableBinderBuilder() {
      this.inserts = new LinkedList<InsertRowBuilder>();
      this.columns = new LinkedList<ColumnBuilder>();
      this.key = new LinkedList<ColumnBuilder>();
   }
   
   public TableBinderElement createElement() {
      TableBinderElement element = new TableBinderElement(this, name, type);

      if (inserts != null) {
         for (InsertRowBuilder builder : inserts) {
            InsertRowElement result = builder.createElement();
            element.addInsert(result);
         }
      }
      if (columns != null) {
         for (ColumnBuilder builder : columns) {
            ColumnElement result = builder.createElement();
            element.addColumn(result);
         }
      }
      if (key != null) {
         for (ColumnBuilder builder : key) {
            ColumnElement result = builder.createElement();
            element.addKey(result);
         }
      }
      return element;
   }
}
