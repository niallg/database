package com.zuooh.database.build.dom.schema;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import com.zuooh.database.build.dom.element.IncludeTableElement;

@Root
public class IncludeTableBuilder {

   @Attribute
   private String file;
   
   public IncludeTableElement createElement() {
      return new IncludeTableElement(file);
   }
}
