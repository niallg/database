package com.zuooh.database.build.dom.element;

import java.util.LinkedList;
import java.util.List;

import com.zuooh.code.load.CodeContext;
import com.zuooh.code.load.CodeVisitor;
import com.zuooh.database.Column;
import com.zuooh.database.bind.TableBinder;
import com.zuooh.database.bind.load.TableDefinition;
import com.zuooh.database.bind.table.TableBuilder;
import com.zuooh.database.build.dom.code.TableBinderSnippet;
import com.zuooh.database.build.dom.schema.TableBinderBuilder;

public class TableBinderElement {

   private final List<InsertRowElement> inserts;
   private final List<ColumnElement> columns;  
   private final List<ColumnElement> key; 
   private final TableBinderBuilder builder;
   private final String name;
   private final Class type;
   
   public TableBinderElement(TableBinderBuilder builder, String name, Class type) {
      this.inserts = new LinkedList<InsertRowElement>();
      this.columns = new LinkedList<ColumnElement>();
      this.key = new LinkedList<ColumnElement>();
      this.builder = builder;
      this.name = name;
      this.type = type;
   }
   
   public void addInsert(InsertRowElement insert) {
      inserts.add(insert);
   }
   
   public void addKey(ColumnElement element) {
      key.add(element);
   }
   
   public void addColumn(ColumnElement element) {
      columns.add(element);
   }
   
   public TableDefinition createTable(TableBuilder tableBuilder) throws Exception {
      TableDefinition tableElement = new TableDefinition(name, type);
      List<String> script = new LinkedList<String>();

      for (ColumnElement element : key) {
         Column column = element.createColumn();
         tableElement.addKey(column);
      }
      for (ColumnElement element : columns) {
         Column column = element.createColumn();
         tableElement.addColumn(column);
      }
      TableBinder tableBinder = tableElement.createBinder(tableBuilder, script);

      for (InsertRowElement insert : inserts) {
         String insertStatement = insert.createRow(tableBinder);

         tableElement.addStatement(insertStatement);
         script.clear();
      }
      return tableElement;
   }

   public String generateTable(TableBuilder tableBuilder, CodeContext application, CodeVisitor visitor) throws Exception {
      TableBinderSnippet snippet = new TableBinderSnippet(builder, name, type);
      TableDefinition tableElement = new TableDefinition(name, type);
      List<String> script = new LinkedList<String>();
      
      for(ColumnElement builder : key) {
         String keyId = builder.generateColumn(application, visitor);
         Column column = builder.createColumn();
         
         tableElement.addKey(column);
         snippet.addKey(keyId);
      }
      for(ColumnElement builder : columns) {
         String columnId = builder.generateColumn(application, visitor);
         Column column = builder.createColumn();
         
         tableElement.addColumn(column);
         snippet.addColumn(columnId);
      }       
      TableBinder tableBinder = tableElement.createBinder(tableBuilder, script);
      
      for(InsertRowElement insert : inserts) {
         String insertStatement = insert.createRow(tableBinder);
         
         tableElement.addStatement(insertStatement);
         script.clear();
      }
      visitor.visitSnippet(snippet);
      return snippet.getVariableId();
   }
}
