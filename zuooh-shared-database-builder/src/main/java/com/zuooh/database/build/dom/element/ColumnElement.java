package com.zuooh.database.build.dom.element;

import com.zuooh.code.load.CodeContext;
import com.zuooh.code.load.CodeVisitor;
import com.zuooh.database.Column;
import com.zuooh.database.build.dom.code.ColumnSnippet;
import com.zuooh.database.build.dom.schema.ColumnBuilder;
import com.zuooh.database.data.DataConstraint;
import com.zuooh.database.data.DataType;

public class ColumnElement {
   
   private ColumnBuilder builder;
   private DataConstraint constraint;
   private String expression;
   private DataType data;
   private String name;
   
   public ColumnElement(ColumnBuilder builder, DataConstraint constraint, DataType data, String expression, String name) {
      this.expression = expression;
      this.constraint = constraint;
      this.builder = builder;
      this.name = name;     
      this.data = data;
   }
   
   public Column createColumn() {       
      return new Column(constraint, data, expression, name, name);
   }
   
   public String generateColumn(CodeContext application, CodeVisitor visitor) throws Exception {
      ColumnSnippet snippet = createSnippet(constraint);
           
      visitor.visitSnippet(snippet);
      return snippet.getVariableId();
   }
   
   protected ColumnSnippet createSnippet(DataConstraint type) throws Exception {
      return new ColumnSnippet(builder, constraint, data, expression, name);      
   }
}
