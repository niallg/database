package com.zuooh.database.build.dom;

import com.zuooh.code.load.CodeContext;
import com.zuooh.database.Database;

public interface DatabaseContext extends CodeContext {
   Database getDatabase();
}
