package com.zuooh.database.build.dom.schema.format;

public class ValueFormatter {

   public String formatValue(Class type, String value) {
      ValueFormat format = resolveFormat(type, value);
      String text = format.formatValue(value);
      
      return text;
   }
   
   private ValueFormat resolveFormat(Class type, String value) {
      if(value != null) {         
         if(type == String.class) {
            return new TextFormat();
         }
         if(type == Boolean.class) {
            return new BooleanFormat();
         }
         if(type == boolean.class) {
            return new BooleanFormat();
         }
         if(type.isEnum()) {
            return new TextFormat();
         }
         return new ValueFormat();
      }
      return new NullFormat();
   }
}
