package com.zuooh.database.build.dom.schema.format;

public class BooleanFormat extends ValueFormat {

   @Override
   public String formatValue(String value) {
      boolean flag = Boolean.parseBoolean(value);
      
      if(flag) {
         return "'true'";
      }
      return "'false'";
   }
}
