package com.zuooh.database.build.dom.schema;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import com.zuooh.database.build.dom.element.ColumnElement;
import com.zuooh.database.data.DataConstraint;
import com.zuooh.database.data.DataType;

@Root
public class ColumnBuilder {
   
   @Attribute(name="constraint", required=false)
   private String constraint;
   
   @Attribute(name="default", required=false)
   private String value;   
   
   @Attribute(name="type", required=false)
   private String type;   
   
   @Attribute(name="name")
   private String name;
   
   public ColumnElement createElement() {
      DataConstraint constraint = createConstraint();
      DataType type = createType();
      
      return new ColumnElement(this, constraint, type, value, name);
   }
   
   private DataConstraint createConstraint() {
      if(constraint != null) {
         return DataConstraint.resolveConstraint(constraint);
      }
      return DataConstraint.OPTIONAL;
   }
   
   private DataType createType() {
      if(type != null) {
         return DataType.resolveType(type);
      }
      return DataType.TEXT;
   }
}
