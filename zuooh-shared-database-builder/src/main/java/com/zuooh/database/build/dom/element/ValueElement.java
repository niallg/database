package com.zuooh.database.build.dom.element;

public class ValueElement {

   private String column;
   private String value;
   
   public ValueElement(String column, String value) {
      this.column = column;
      this.value = value;
   }
   
   public String getColumn() {
      return column;
   }
   
   public String getValue() {
      return value;
   }  
}
