package com.zuooh.database.build.dom.element;

import java.io.InputStream;

import org.simpleframework.xml.Serializer;

import com.zuooh.code.load.CodeContext;
import com.zuooh.common.FileSystem;
import com.zuooh.database.build.dom.schema.TableBinderBuilder;

public class IncludeTableElement {

   private final String file;
   
   public IncludeTableElement(String file) {     
      this.file = file;
   }
   
   public String getFile() {
      return file;
   }   
   
   public TableBinderElement createElement(CodeContext context) {
      try {
         FileSystem fileSystem = context.getFileSystem();
         Serializer serializer = context.getSerializer();
         InputStream source = fileSystem.openFile("database/" + file); // leave this like this for consistency         
         TableBinderBuilder builder = serializer.read(TableBinderBuilder.class, source);
         
         return builder.createElement();
      } catch(Exception e) {
         throw new IllegalStateException("Could not load table file " + file, e);
      }
   }
}
