package com.zuooh.database.build.dom.schema.format;

public class TextFormat extends ValueFormat {
   
   private final char quote;
   
   public TextFormat() {
      this('\'');
   }
   
   public TextFormat(char quote) {
      this.quote = quote;
   }
   
   @Override
   public String formatValue(String value) {
      return quote + value + quote;      
   }
}
