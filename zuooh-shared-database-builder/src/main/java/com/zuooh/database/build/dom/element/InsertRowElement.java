package com.zuooh.database.build.dom.element;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.zuooh.database.Column;
import com.zuooh.database.Schema;
import com.zuooh.database.bind.TableBinder;
import com.zuooh.database.build.dom.schema.format.ValueFormatter;
import com.zuooh.database.data.DataType;

public class InsertRowElement {
   
   private List<ValueElement> values;
   
   public InsertRowElement() {
      this.values = new LinkedList<ValueElement>();
   }
   
   public void addValue(ValueElement element) {
      values.add(element);
   }
   
   public String createRow(TableBinder table) throws Exception {
      Schema schema = table.schema();
      String name = table.name();
      Map<String, String> parameters = createParameters(schema, name);
      
      return createStatement(schema, parameters, name);
   }
   
   private Map<String, String> createParameters(Schema schema, String table) throws Exception {
      Map<String, String> parameters = new HashMap<String, String>();
      
      for(ValueElement element : values) {
         String name = element.getColumn();
         String value = element.getValue();
         Column column = schema.getColumn(name);
         
         if(column == null) {
            throw new IllegalStateException("Row references unknown column '" + name + "' in table " + table);
         }
         parameters.put(name, value);
      }
      return parameters;
   }
   
   private String createStatement(Schema schema, Map<String, String> parameters, String table) throws Exception {
      StringBuilder builder = new StringBuilder();       
      ValueFormatter formatter = new ValueFormatter();      
    
      builder.append("insert or ignore into ");
      builder.append(table);
      builder.append(" (");
      
      List<String> columns = schema.getColumns();
      
      for(String column : columns) {
         Column value = schema.getColumn(column);
         int index = value.getIndex();
         
         if(index > 0) {
            builder.append(", ");
         }
         builder.append(column);
      }
      builder.append(") values (");
      
      for(String column : columns) {
         Column value = schema.getColumn(column);
         DataType data = value.getDataType();
         String title = value.getTitle();
         String token = parameters.get(title);
         Class type = data.getType();
         String text = formatter.formatValue(type, token);         
         int index = value.getIndex();
         
         if(index > 0) {
            builder.append(", ");
         }
         builder.append(text);
      }
      builder.append(")"); 

      return builder.toString();
   }
}
