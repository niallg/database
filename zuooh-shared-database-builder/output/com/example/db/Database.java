package com.example.db;

import com.zuooh.application.Application;
import com.zuooh.code.load.SourceCodeContext;
import com.zuooh.code.load.SourceCodeInstantiator;
import com.zuooh.database.bind.table.ColumnType;
import com.zuooh.database.build.dom.ColumnElement;
import com.zuooh.database.build.dom.DatabaseBinderElement;
import com.zuooh.database.build.dom.TableBinderElement;
import com.zuooh.database.build.load.DatabaseFileSystemLoaderTest$ExampleObject;
import java.lang.Class;
import java.lang.String;
import static com.zuooh.database.bind.table.ColumnType.*;

public class Database implements SourceCodeInstantiator<DatabaseBinderBuilder, Application> {
   
   public static final long TIMESTAMP = 1405603776806L;
   public static final String DATE_CREATED = "Thu, 17 Jul 2014 23:29:36 EST";
   public static final String[] FILES = {
      "database.xml"};
   public static final ColumnElement COLUMN_COLOR = new ColumnElement(OPTIONAL, "color", java.lang.String.class);
   public static final ColumnElement COLUMN_THEME = new ColumnElement(OPTIONAL, "theme", java.lang.String.class);
   
   private TableBinderElement table1893488603;
   private DatabaseBinderElement database302785728;
   
   public Database() {
      super();
   }
   
   @Override
   public DatabaseBinderBuilder createObject(Application application) throws Exception {
      table1893488603 = new TableBinderElement("themeDefinition", com.zuooh.database.build.load.DatabaseFileSystemLoaderTest$ExampleObject.class);
      table1893488603.addKey(COLUMN_COLOR);
      table1893488603.addKey(COLUMN_THEME);
      database302785728 = new DatabaseBinderElement("database", 1);
      database302785728.addTable(table1893488603);
      return database302785728;
   }
}
