package com.zuooh.database.bind.load;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.database.Database;
import com.zuooh.database.DatabaseConnection;
import com.zuooh.database.bind.DatabaseBinder;
import com.zuooh.database.bind.TableBinder;
import com.zuooh.database.bind.table.TableBuilder;
import com.zuooh.database.bind.table.attribute.AttributeTableBuilder;

public class DatabaseDefinition implements DatabaseSource {
   
   private static final Logger LOG = LoggerFactory.getLogger(DatabaseDefinition.class);
   
   private final Map<String, TableBinder> binders;
   private final List<TableDefinition> tables;
   private final List<String> statements;
   private final List<String> files;
   private final String name;
   private final int version;
   
   public DatabaseDefinition(String name, int version) {
      this.binders = new ConcurrentHashMap<String, TableBinder>();
      this.tables = new LinkedList<TableDefinition>();
      this.statements = new LinkedList<String>();
      this.files = new LinkedList<String>();      
      this.version = version;
      this.name = name;
   }
   
   public Iterator<TableBinder> listBinders() throws Exception {
      return binders.values().iterator();
   }
   
   @Override
   public DatabaseBinder createDatabase(Database database) throws Exception {
      Map<Object, TableBinder> registry = new HashMap<Object, TableBinder>();   
      TableBuilder builder = new AttributeTableBuilder(database, version);
      List<String> script = new LinkedList<String>();   
      
      for(TableDefinition definition : tables) {
         TableBinder binder = definition.createBinder(builder, script);
         Class type = definition.getType();
         String name = definition.getName();
         
         if(registry.containsKey(name)) {
            throw new IllegalStateException("Table of name '" + name + "' has already been declared");
         }
         binders.put(name, binder);
         registry.put(name, binder); 
         registry.put(type, binder);         
         script.clear();
      }
      DatabaseConnection connection = database.getConnection();
      
      try {
         for(String statement : statements) {
            try {
               connection.executeStatement(statement);
            } catch(Exception cause) {
               String message = cause.getMessage();
               Class type = cause.getClass();
               String exception = type.getSimpleName();
               
               LOG.info("Error of type '" + exception + "' occurred executing '" + statement + "', reason was '" + message + "'");
            }
         }
         return new RegistryBinder(database, registry);
      } finally {
         connection.closeConnection();
      }
   }
   
   public List<TableDefinition> getTables() {
      return tables;
   }
   
   public void addTable(TableDefinition table) {
      tables.add(table);
   }
   
   public void addResource(String file) {
      files.add(file);
   }   
   
   public void addStatement(String statement) {
      statements.add(statement);
   }   
   
   public String getName() {
      return name;
   }
   
   public int getVersion() {
      return version;
   }
   
   private static class RegistryBinder implements DatabaseBinder {
      
      private final Map<Object, TableBinder> registry;
      private final Database database;
      
      public RegistryBinder(Database database, Map<Object, TableBinder> registry) {
         this.database = database;
         this.registry = registry;
      }
      
      @Override
      public TableBinder withTable(String name) {
         TableBinder binder = registry.get(name);
         
         if(binder == null) {
            throw new IllegalArgumentException("No table found for " + name);
         }
         return binder;
      } 

      @Override
      public TableBinder withTable(Class type) {
         TableBinder binder = registry.get(type);
         
         if(binder == null) {
            throw new IllegalArgumentException("No table found for " + type);
         }
         return binder;
      }    
      
      public Database withDatabase() {
         return database;
      }
   }
}
