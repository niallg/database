package com.zuooh.database.bind.load;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.zuooh.database.Column;
import com.zuooh.database.bind.TableBinder;
import com.zuooh.database.bind.table.TableBuilder;

public class TableDefinition {
   
   private List<String> statements;
   private List<Column> columns;
   private List<Column> keys;
   private String name;
   private Class type;
   
   public TableDefinition(String name, Class type) {
      this.statements = new ArrayList<String>();
      this.columns = new ArrayList<Column>();
      this.keys = new ArrayList<Column>();
      this.name = name;      
      this.type = type;
   }

   public TableBinder createBinder(TableBuilder builder, List<String> script) {
      List<Column> definition = new ArrayList<Column>();
      List<String> names = new ArrayList<String>();
      
      for(String statement : statements) {
         if(script.contains(statement)) {
            throw new IllegalStateException("Attempting to add statement '" + statement + "' which already exists");
         }
         script.add(statement);
      }
      for(Column column : keys) {
         String name = column.getTitle();
              
         definition.add(column);
         names.add(name);
      }
      for(Column column : columns) {
         String name = column.getTitle();
              
         if(names.contains(name)) {
            throw new IllegalStateException("Column '" + name + "' already declared as a key");
         }
         definition.add(column);
      }
      return builder.createTable(name, type, names, definition);
   }   
   
   public Class getType() {
      return type;
   }
   
   public String getName() {
      return name;
   }
   
   public void addStatement(String statement) {
      statements.add(statement);
   }
   
   public void addKey(Column key) {
      keys.add(key);
   }
   
   public void addColumn(Column column) {
      columns.add(column);
   }   

}
