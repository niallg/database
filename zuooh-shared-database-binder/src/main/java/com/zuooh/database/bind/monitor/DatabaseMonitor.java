package com.zuooh.database.bind.monitor;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.zuooh.container.annotation.ManagedOperation;
import com.zuooh.container.annotation.ManagedOperationParameter;
import com.zuooh.container.annotation.ManagedOperationParameters;
import com.zuooh.container.annotation.ManagedResource;
import com.zuooh.database.Database;
import com.zuooh.database.DatabaseConnection;
import com.zuooh.database.Record;
import com.zuooh.database.ResultIterator;
import com.zuooh.database.Statement;
import com.zuooh.database.bind.DatabaseBinder;

@ManagedResource("Execute SQL queries")
public class DatabaseMonitor {
   
   private final DatabaseBinder binder;

   public DatabaseMonitor(DatabaseBinder binder) {
      this.binder = binder;
   }
   
   @ManagedOperation("Execute a query")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="expression", description="SQL query")
   })   
   public String executeQuery(String expression) throws Exception {
      return executeQuery(expression, false);
   }
   
   @ManagedOperation("Execute a query")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="expression", description="SQL query"),
      @ManagedOperationParameter(name="rows", description="Show rows or columns")
   })   
   public String executeQuery(String expression, boolean rows) throws Exception {
      Database database = binder.withDatabase();
      DatabaseConnection connection = database.getConnection();
      
      try {
         Statement statement = connection.prepareStatement(expression);
         ResultIterator<Record> iterator = statement.execute();
         
         if(rows) {
            return showResultRows(expression, iterator);
         }
         return showResultColumns(expression, iterator);
      } finally {
         connection.closeConnection();
      }
   }
   
   private String showResultColumns(String expression, ResultIterator<Record> iterator) throws Exception {
      Map<String, List<String>> rows = new LinkedHashMap<String, List<String>>();
      StringBuilder builder = new StringBuilder();
      
      builder.append("<h2>");
      builder.append(expression);
      builder.append("</h2>\n");
      builder.append("<table border='1'>\n");
      
      while(iterator.hasMore()) {
         Record record = iterator.next();
         Set<String> columns = record.getColumns();
         
         if(rows.isEmpty()) {
            for(String column : columns) {
               List<String> row = new LinkedList<String>();
               rows.put(column, row);
            }
         }
         for(String column : columns) {
            String value = record.getString(column);
            List<String> row = rows.get(column);
            
            row.add(value);
         }
      }
      Set<String> names = rows.keySet();
      
      for(String name : names) {         
         List<String> cells = rows.get(name);
         
         builder.append("<tr>");
         builder.append("<td><b>");
         builder.append(name);
         builder.append("</b></td>");
         
         for(String cell : cells) {
            builder.append("<td>");
            builder.append(cell);
            builder.append("</td>");
         }
         builder.append("</tr>\n");
      }
      builder.append("</table>");
      return builder.toString();
   }
   
   private String showResultRows(String expression, ResultIterator<Record> iterator) throws Exception {
      StringBuilder builder = new StringBuilder();
      
      while(iterator.hasMore()) {
         Record record = iterator.next();
         Set<String> columns = record.getColumns();
         int length = builder.length();
         
         if(length == 0) {
            builder.append("<h2>");
            builder.append(expression);
            builder.append("</h2>\n");
            builder.append("<table border='1'>\n");
            
            for(String column : columns) {
               builder.append("<th>");
               builder.append(column);
               builder.append("</th>\n");
            }
         }
         builder.append("<tr>");
         
         for(String column : columns) {
            String value = record.getString(column);
          
            builder.append("<td>");
            builder.append(value);
            builder.append("</td>");
         }
         builder.append("</tr>\n");
      }
      builder.append("</table>");
      return builder.toString();
   }
}
