package com.zuooh.database.bind.load;

import com.zuooh.database.Database;
import com.zuooh.database.bind.DatabaseBinder;

public interface DatabaseSource {
   DatabaseBinder createDatabase(Database database) throws Exception;
}
