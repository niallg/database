package com.zuooh.database.bind.table.attribute;

import com.zuooh.attribute.ObjectBuilder;
import com.zuooh.attribute.ReflectionBuilder;
import com.zuooh.attribute.SerializationBuilder;
import com.zuooh.attribute.SerializationStreamBuilder;

public class RowBuilder implements ObjectBuilder {
   
   private final ObjectBuilder[] builders;
   
   public RowBuilder() {
      this.builders = new ObjectBuilder[3];
      this.builders[0] = new ReflectionBuilder();
      this.builders[1] = new SerializationBuilder();
      this.builders[2] = new SerializationStreamBuilder();
   }
  
   @Override
   public Object createInstance(Class type) {
      for (ObjectBuilder factory : builders) {
         Object value = factory.createInstance(type);

         if (value != null) {
            return value;
         }
      }
      return null;
   }

}
