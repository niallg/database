package com.zuooh.database.bind;

import com.zuooh.database.Database;

public interface DatabaseBinder {
   Database withDatabase();
   <T> TableBinder<T> withTable(String name);
   <T> TableBinder<T> withTable(Class<T> type);
}
