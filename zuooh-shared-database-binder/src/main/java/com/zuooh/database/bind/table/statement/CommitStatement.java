package com.zuooh.database.bind.table.statement;

import com.zuooh.database.Database;
import com.zuooh.database.DatabaseConnection;
import com.zuooh.database.Schema;
import com.zuooh.database.bind.table.TableContext;

public class CommitStatement<T> {

   private final TableContext<T> context;
   private final Database database; 

   public CommitStatement(Database database, TableContext<T> context) {
      this.database = database;   
      this.context = context;
   }
   
   public String compile() throws Exception {
      String table = context.getName();
      Schema schema = context.getSchema();
      int columnCount = schema.getCount();
      
      if(columnCount <= 0) {
         throw new IllegalStateException("Table '" + table + "' has " + columnCount + " columns");
      }
      StringBuilder builder = new StringBuilder();
      
      builder.append("commit");
      
      if(table != null) {
         builder.append(" on ");
         builder.append(table);
      }            
      return builder.toString();
   }
   
   public String execute() throws Exception {
      String statement = compile();
      
      if(statement != null) {
         DatabaseConnection connection = database.getConnection();
         
         try {
            connection.executeStatement(statement);
         } finally {
            connection.closeConnection();
         }
      }
      return statement;
   }   
}
