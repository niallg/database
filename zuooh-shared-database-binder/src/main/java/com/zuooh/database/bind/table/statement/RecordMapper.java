package com.zuooh.database.bind.table.statement;

import com.zuooh.database.Record;

public interface RecordMapper<T> {
   Record fromObject(T object) throws Exception;
   T toObject(Record record) throws Exception;
}
