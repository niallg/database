package com.zuooh.database.bind.table.attribute;

import static com.zuooh.database.data.DataType.TEXT;

import java.util.List;

import com.zuooh.database.Column;
import com.zuooh.database.ColumnSeries;
import com.zuooh.database.data.DataConstraint;
import com.zuooh.database.data.DataType;
import com.zuooh.database.function.DefaultValue;

public class ColumnMerger {
   
   private final DataType basic;
   
   public ColumnMerger() {
      this(TEXT);
   }
   
   public ColumnMerger(DataType basic) {
      this.basic = basic;
   }

   public void merge(ColumnSeries series, Column column) {
      String title = column.getTitle();
      List<String> columns = series.getColumns();
      
      if(columns.contains(title)) {
         Column existing = series.getColumn(title);   
         
         if(existing != null) {
            DefaultValue value = existing.getDefaultValue();
            DataConstraint constraint = existing.getDataConstraint();
            DataType override = column.getDataType();
            DataType current = existing.getDataType();
            String expression = value.getExpression();
            String name = column.getName();
            int index = existing.getIndex();
            
            if(current == basic) {
               column = new Column(constraint, override, expression, name, title, index);
            } else {
               column = new Column(constraint, current, expression, name, title, index);
            }
         }
      }
      series.addColumn(column);
   }
}
