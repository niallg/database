package com.zuooh.database.bind.table;

import java.util.List;

import com.zuooh.database.Column;
import com.zuooh.database.bind.TableBinder;

public interface TableBuilder {
   TableBinder createTable(String name, Class type, String key);
   TableBinder createTable(String name, Class type, List<String> key);
   TableBinder createTable(String name, Class type, String key, List<Column> columns);   
   TableBinder createTable(String name, Class type, List<String> key, List<Column> columns);
}
