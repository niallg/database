package com.zuooh.database.build.load;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.Strategy;
import org.simpleframework.xml.strategy.TreeStrategy;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.build.generate.CodeCriteria;
import com.zuooh.code.ClassNameResolver;
import com.zuooh.code.load.ClassResultLoader;
import com.zuooh.code.load.ResultLoader;
import com.zuooh.common.FileSystem;
import com.zuooh.database.Database;
import com.zuooh.database.bind.load.DatabaseDefinition;
import com.zuooh.database.build.dom.DatabaseContext;

public class DatabaseCodeCriteria extends CodeCriteria {

   public DatabaseCodeCriteria(String packageName, String directory) {
      super(DatabaseDefinition.class, packageName, directory);
   }

   @Override
   protected ResultLoader createFileLoader(MobileApplication application, ClassNameResolver nameResolver) {
      DatabaseContext context = new DatabaseContextDelegate(application);      
      DatabaseFileLoader loader = new DatabaseFileLoader(context, directory);
      
      return loader;
   }

   @Override
   protected ResultLoader createClassLoader(MobileApplication application, ClassNameResolver nameResolver) {
      return new ClassResultLoader(nameResolver, application);
   }
   
   private static class DatabaseContextDelegate implements DatabaseContext {      
      
      private final MobileApplication application;
      private final Serializer serializer;
      private final Strategy strategy;
      
      public DatabaseContextDelegate(MobileApplication application) {
         this.strategy = new TreeStrategy("object", "length");
         this.serializer = new Persister(strategy);
         this.application = application;
      }
      
      @Override
      public Database getDatabase() {
         return application.getDatabase();
      }

      @Override
      public FileSystem getFileSystem() {
         return application.getFileSystem();
      }

      @Override
      public Serializer getSerializer() {
         return serializer;
      }
   }
}

