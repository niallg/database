package com.zuooh.database.build.load;

import com.zuooh.application.build.load.FileInstantiator;
import com.zuooh.application.build.load.FileResultLoader;
import com.zuooh.code.load.CodeProcessor;
import com.zuooh.code.load.ResultLoader;
import com.zuooh.database.build.dom.DatabaseContext;
import com.zuooh.database.build.dom.schema.DatabaseBinderBuilder;

public class DatabaseFileLoader implements ResultLoader {

   private final FileInstantiator<DatabaseBinderBuilder> instantiator;   
   private final FileResultLoader<DatabaseBinderBuilder> loader;  

   public DatabaseFileLoader(DatabaseContext context, String directory) {
      this.instantiator = new DatabaseFileInstantiator(context);
      this.loader = new FileResultLoader<DatabaseBinderBuilder>(context, instantiator, DatabaseBinderBuilder.class, directory);     
   }

   @Override
   public Object loadResult(String source) throws Exception {
      return loader.loadResult(source);
   }

   @Override
   public boolean clearResult(String source) throws Exception {
      return loader.clearResult(source);
   }

   @Override
   public CodeProcessor loadBuilder(String file) throws Exception {
      return loader.loadBuilder(file);
   }
}
