package com.zuooh.database.build.load;

import com.zuooh.application.build.load.FileInstantiator;
import com.zuooh.database.bind.load.DatabaseDefinition;
import com.zuooh.database.build.dom.DatabaseContext;
import com.zuooh.database.build.dom.element.DatabaseBinderElement;
import com.zuooh.database.build.dom.schema.DatabaseBinderBuilder;

public class DatabaseFileInstantiator implements FileInstantiator<DatabaseBinderBuilder> {
   
   private final DatabaseContext context;   

   public DatabaseFileInstantiator(DatabaseContext context) {
      this.context = context;
   }

   @Override
   public DatabaseDefinition createObject(DatabaseBinderBuilder builder) throws Exception {
      DatabaseBinderElement element = builder.createElement();
      return element.createDatabase(context);
   }
}
