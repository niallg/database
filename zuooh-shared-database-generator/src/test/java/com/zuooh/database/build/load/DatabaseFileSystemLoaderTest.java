package com.zuooh.database.build.load;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.simpleframework.xml.core.Persister;

import com.zuooh.application.AddressBook;
import com.zuooh.application.AdvertManager;
import com.zuooh.application.DialogManager;
import com.zuooh.application.DocumentBuilder;
import com.zuooh.application.FontManager;
import com.zuooh.application.MobileApplication;
import com.zuooh.application.ResourceState;
import com.zuooh.application.Screen;
import com.zuooh.application.ServiceLauncher;
import com.zuooh.application.SoundManager;
import com.zuooh.application.Window;
import com.zuooh.application.build.generate.CodeGeneratorLoader;
import com.zuooh.application.image.ImageBuilder;
import com.zuooh.application.keypad.KeyPad;
import com.zuooh.application.load.database.DatabaseLoader;
import com.zuooh.application.load.page.PageLoader;
import com.zuooh.application.load.tile.TileLoader;
import com.zuooh.application.store.ProductStore;
import com.zuooh.common.FileSystem;
import com.zuooh.common.process.ProcessMonitor;
import com.zuooh.common.task.TaskScheduler;
import com.zuooh.container.Container;
import com.zuooh.database.Database;
import com.zuooh.database.DatabaseConnection;
import com.zuooh.database.Statement;
import com.zuooh.database.bind.load.DatabaseSource;
import com.zuooh.script.ScriptExecutor;

public class DatabaseFileSystemLoaderTest extends TestCase {
   
   private static final String DATABASE =
   "<database version='1' name='database'>\r\n"+
   "  <include file='database/table.xml' />\r\n"+
   "</database>\r\n";
         
   private static final String TABLE =
   "<table name='themeDefinition' type='com.zuooh.database.build.load.DatabaseFileSystemLoaderTest$ExampleObject'>\r\n"+
   "  <schema>\r\n"+
   "    <key>\r\n"+
   "      <column name='color' />\r\n"+
   "      <column name='theme' />\r\n"+
   "    </key>\r\n"+
   "  </schema>\r\n"+
   "</table>\r\n";
         
   private static class ExampleObject implements Serializable {
      String color;
      String theme;
   }   
   
   public void testDatabaseLoader() throws Exception {
      DatabaseCodeCriteria spec = new DatabaseCodeCriteria("com.example.db", "database");
      Map<String, String> files = new HashMap<String, String>();
      
      files.put("database/database.xml", DATABASE);
      files.put("database/table.xml", TABLE);
      files.put("database/database/table.xml", TABLE);
      
      Database database = new MockDatabase();
      FileSystem fileSystem = new MockFileSystem(files);
      MobileApplication application = new MockMobileApplication(database, fileSystem); 
      String[] paths = new String[]{"output"};
      CodeGeneratorLoader generator = new CodeGeneratorLoader(application, spec, paths);
      DatabaseSource binder = (DatabaseSource)generator.loadResult("database.xml");
      
      assertNotNull(binder);      
   }
   
   public static class MockDatabase implements Database {

      @Override
      public DatabaseConnection getConnection() throws Exception {
         return new MockDatabaseConnection();
      }
   }
   
   public static class MockDatabaseConnection implements DatabaseConnection {

      @Override
      public void executeStatement(String expression) throws Exception {}

      @Override
      public Statement prepareStatement(String expression) throws Exception {
         return null;
      }

      @Override
      public Statement prepareStatement(String expression, boolean cache) throws Exception {
         return null;
      }

      @Override
      public void closeConnection() throws Exception {}    
      
   }
   
   public static class MockFileSystem implements FileSystem {

      private final Map<String, String> files;

      public MockFileSystem(Map<String, String> files) {
         this.files = files;
      }

      @Override
      public InputStream openFile(String file) {
         String content = files.get(file);

         try {
            byte[] bytes = content.getBytes("UTF-8");
            return new ByteArrayInputStream(bytes);
         } catch(Exception e) {
            throw new IllegalStateException("Could not find file '" + file + "'", e);
         }
      }

      @Override
      public OutputStream createExternalFile(String file) {
         return System.out;
      }

      @Override
      public InputStream openExternalFile(String file) {
         return openFile(file);
      }

      @Override
      public String[] listExternalFiles(String filter) {
         return null;
      }

      @Override
      public String[] listFiles(String filter) {
         return null;
      }

      @Override
      public boolean deleteExternalFile(String file) {
         return false;
      }

      @Override
      public boolean deleteFile(String file) {
         return false;
      }

      @Override
      public long lastModified(String file) {
         return 0;
      }

      @Override
      public long lastExternalModified(String file) {
         return 0;
      }

      @Override
      public long freeExternalSpace() {
         return 0;
      }

      @Override
      public long sizeOfExternalFile(String file) {
         return 0;
      }

      @Override
      public boolean touchExternalFile(String file) {
         return false;
      }

      @Override
      public byte[] loadFile(String file) {
         return null;
      }

      @Override
      public byte[] loadExternalFile(String file) {
         return null;
      }

      @Override
      public long sizeOfFile(String fileName) {
         return 0;
      }
   }

   public static class MockMobileApplication implements MobileApplication {
      
      private final Persister persister;
      private final FileSystem fileSystem;
      private final Database database;
      
      public MockMobileApplication(Database database, FileSystem fileSystem) {
         this.persister = new Persister();
         this.database = database;
         this.fileSystem = fileSystem;
      }

      @Override
      public ProductStore getProductStore() {
         return null;
      }
      
      @Override
      public AddressBook getAddressBook(){
         return null;
      }

      @Override
      public KeyPad getKeyPad() {
         return null;
      }

      @Override
      public Screen getScreen() {
         return null;
      }

      @Override
      public Window getWindow() {
         return null;
      }

      @Override
      public FileSystem getFileSystem() {
         return fileSystem;
      }

      @Override
      public ResourceState getResourceState(String name) {
         return null;
      }

      @Override
      public ScriptExecutor getScriptExecutor() {
         return null;
      }

      @Override
      public ProcessMonitor getProcessMonitor() {
         return null;
      }

      @Override
      public Container getContainer() {
         return null;
      }

      @Override
      public PageLoader getPageLoader() {
         return null;
      }

      @Override
      public TileLoader getTileLoader() {
         return null;
      }

      @Override
      public DatabaseLoader getDatabaseLoader() {
         return null;
      }

      @Override
      public TaskScheduler getTaskScheduler() {
         return null;
      }

      @Override
      public DocumentBuilder getDocumentBuilder() {
         return null;
      }

      @Override
      public ImageBuilder getImageBuilder() {
         return null;
      }

      @Override
      public FontManager getFontManager() {
         return null;
      }

      @Override
      public DialogManager getDialogManager() {
         return null;
      }

      @Override
      public Database getDatabase() {
         return database;
      }

      @Override
      public AdvertManager getAdvertManager() {
         return null;
      }

      @Override
      public SoundManager getSoundManager() {
         return null;
      }

      @Override
      public ServiceLauncher getServiceLauncher() {
         return null;
      }      
   }
}
