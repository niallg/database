package com.zuooh.database.sql.build;

import com.zuooh.database.sql.Query;
import com.zuooh.database.sql.QueryConverter;
import com.zuooh.database.sql.parse.QueryParser;

public class QueryProcessor<T> {

   private final QueryConverter<T> converter;
   private final QueryParser parser;
   
   public QueryProcessor(QueryConverter<T> converter) {
      this.parser = new QueryParser();
      this.converter = converter;
   }
   
   public T process(String text) {
      Query command = parser.parse(text);
      
      if(command != null) {
         return converter.convert(command);
      }
      return null;
   }
}
