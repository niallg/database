package com.zuooh.database.sql.build;

import com.zuooh.database.sql.Query;
import com.zuooh.database.sql.QueryConverter;

public class IdentityConverter implements QueryConverter<Query> {

   @Override
   public Query convert(Query query) {
      return query;
   }

}
