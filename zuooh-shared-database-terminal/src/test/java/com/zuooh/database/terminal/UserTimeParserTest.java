package com.zuooh.database.terminal;

import junit.framework.TestCase;

public class UserTimeParserTest extends TestCase {
   
   public void testUserTime() throws Exception {
      UserTimeParser parser = new UserTimeParser();
      UserTime time = parser.parseTime("Fri Jun 12 2015 19:20:06 GMT+1000 (AUS Eastern Standard Time)");
      
      assertEquals(time.getTimeZone().getRawOffset(), 36000000);
      assertEquals(time.getText(), "Fri Jun 12 2015 19:20:06 GMT+1000 (AUS Eastern Standard Time)");
   }

}
