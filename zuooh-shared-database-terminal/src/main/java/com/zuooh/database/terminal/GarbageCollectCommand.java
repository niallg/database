package com.zuooh.database.terminal;

import java.io.PrintStream;

import com.zuooh.common.memory.MemoryUnit;

public class GarbageCollectCommand implements Command {

   @Override
   public boolean execute(UserSession session, PrintStream console, String expression) throws Exception{
      double memoryAllocated = Runtime.getRuntime().totalMemory();
      double memoryFreeBefore = Runtime.getRuntime().freeMemory();            
     
      System.gc();

      double memoryFreeAfter = Runtime.getRuntime().freeMemory();
      double memoryFreed = memoryFreeAfter - memoryFreeBefore;
      double percentageFreed = (memoryFreed / memoryAllocated) * 100f;
      
      if(memoryFreed > 0) {
         console.println("GC " + MemoryUnit.format(memoryFreed) + " - " + Math.round(percentageFreed) + "%");
      } else {
         console.println("GC 0 B - 0%");
      }
      return true;
   }
}
