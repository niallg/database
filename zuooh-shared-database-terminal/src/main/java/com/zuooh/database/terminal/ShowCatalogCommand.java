package com.zuooh.database.terminal;

import java.io.PrintStream;
import java.util.Set;

import com.zuooh.database.imdb.Catalog;
import com.zuooh.database.imdb.Table;
import com.zuooh.database.imdb.TableModel;

public class ShowCatalogCommand implements Command {

   @Override
   public boolean execute(UserSession session, PrintStream out, String expression)throws Exception {
      Catalog catalog = session.getCatalog();
      
      if(catalog != null) {
         Set<String> tables = catalog.listTables();
         ConsoleTable console = ConsoleTableBuilder.create("table", "rows");
                      
         for(String entry : tables){
            Table tab = catalog.findTable(entry);
            TableModel model = tab.getModel();
            int rows = model.size();
            
            console.add(entry, rows);
         }
         console.draw(out);
      } else {
         out.println("No catalog found");
      }
      return true;      
   }

}
