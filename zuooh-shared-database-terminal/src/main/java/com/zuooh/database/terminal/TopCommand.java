package com.zuooh.database.terminal;

import java.io.PrintStream;
import java.lang.management.ManagementFactory;
import java.util.Map;
import java.util.Set;

import com.sun.management.ThreadMXBean;
import com.zuooh.common.memory.MemoryUnit;

public class TopCommand implements Command {

   private final ThreadMXBean analyzer;

   public TopCommand() {
      this.analyzer = (ThreadMXBean) ManagementFactory.getThreadMXBean();      
   }

   @Override
   public boolean execute(UserSession session, PrintStream console, String expression) throws Exception {
      Map<Thread, StackTraceElement[]> traces = Thread.getAllStackTraces();
      Set<Thread> threads = traces.keySet();
      Thread thisThread = Thread.currentThread();
      long thisKey = thisThread.getId();

      ConsoleTable table = ConsoleTableBuilder.create("id", "name", "state", "allocated", "depth");

      for (Thread thread : threads) {
         StackTraceElement[] elements = traces.get(thread);
         Thread.State state = thread.getState();
         String name = thread.getName();
         ConsoleRow row = table.add();
         long key = thread.getId();
         
         if(thisKey == key) {
            row.set("id", key + "*");
         } else {
            row.set("id", key);
         }
         long bytes = analyzer.getThreadAllocatedBytes(key);
         String memory = MemoryUnit.format(bytes);
         
         row.set("name", name);
         row.set("state", state);
         row.set("allocated", memory);
         row.set("depth", elements.length);
      }
      table.draw(console);
      return true;
   }

}
