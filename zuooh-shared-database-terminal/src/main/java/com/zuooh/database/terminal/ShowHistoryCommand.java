package com.zuooh.database.terminal;

import java.io.PrintStream;
import java.util.List;

public class ShowHistoryCommand implements Command {

   @Override
   public boolean execute(UserSession session, PrintStream console, String expression) throws Exception{
      List<String> commands = session.getHistory();
      
      if(!commands.isEmpty()) {
         int count = commands.size();
         
         for(int i = 0; i < count; i++) {
            String command = commands.get(i);         
        
            console.print(i);
            console.print(") ");
            console.println(command);
         }
      } else {
         console.println("No history");
      }
      return true;
   }

}
