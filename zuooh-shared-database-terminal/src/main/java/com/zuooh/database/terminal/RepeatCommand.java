package com.zuooh.database.terminal;

import java.io.PrintStream;
import java.util.List;

public class RepeatCommand implements Command {
   
   private final GoCommand command;
   
   public RepeatCommand(){
      this.command = new GoCommand();
   }

   @Override
   public boolean execute(UserSession session, PrintStream console, String expression) throws Exception{
      List<String> commands = session.getHistory();
      
      if(!commands.isEmpty() && !expression.isEmpty()) {
         String suffix = expression.trim().substring(1);
         int match = commands.size(); // last one
         int index = 1;
         
         if(!suffix.isEmpty()){
            match = Integer.parseInt(suffix);
         }
         for(String text : commands){
            if(match == index){
               StringBuilder builder = session.getBuffer();
               
               builder.setLength(0);
               builder.append(text);
               return command.execute(session, console, text);               
            }
            index++;
         }
         console.println("Could not repeat '" + match + "'");
      } else {
         console.println("No history");
      }
      return true;
   }
}
