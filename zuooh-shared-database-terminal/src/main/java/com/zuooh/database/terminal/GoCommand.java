package com.zuooh.database.terminal;

import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.List;

import com.zuooh.database.Database;
import com.zuooh.database.DatabaseConnection;

public class GoCommand implements Command {

   private final QueryRequestBuilder builder;
   private final QueryExecutor executor;

   public GoCommand() {
      this.executor = new QueryExecutor();
      this.builder = new QueryRequestBuilder();
   }

   @Override
   public boolean execute(UserSession session, PrintStream out, String text) throws Exception {
      List<String> commands = session.getHistory();
      Database database = session.getDatabase();
      StringBuilder buffer = session.getBuffer();
      String source = buffer.toString();
      
      buffer.setLength(0);    
     
      try {
         DatabaseConnection connection = database.getConnection();
         QueryRequest request = builder.createRequest(source);
         QueryResult result = executor.execute(session, request);
         DecimalFormat formatter = session.getFormat();
         String expression = request.getExpression();
         String output = result.getOutput();
         String memory = result.getMemory();         
         long duration = result.getDuration();
         int repeat = request.getRepeat();
         int rows = result.getRows();

         out.println("> " + expression);
         out.println(output);      
         out.print("Finished (Time " + duration + " ms) (Required " + memory + ")");
         
         if (rows > 0) {
            out.print(" (Result " + rows + " rows)");
         }
         if (repeat > 1) {
            out.print(" (Repeat " + formatter.format(repeat) + " times)");
         }
         out.println();
         commands.add(expression);
         connection.closeConnection();
      } catch (Exception cause) {
         cause.printStackTrace(out);
      }
      return true;
   }

}
