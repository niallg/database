package com.zuooh.database.terminal;

import java.io.PrintStream;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;

import com.zuooh.common.time.DateTime;
import com.zuooh.common.time.DateTime.Duration;

public class UpTimeCommand implements Command {
   
   private final RuntimeMXBean runtime;
   
   public UpTimeCommand(){
      this.runtime = ManagementFactory.getRuntimeMXBean();
   }

   @Override
   public boolean execute(UserSession session, PrintStream console, String expression) throws Exception{
      long start = runtime.getStartTime();
      DateTime now = DateTime.now();
      Duration duration = now.timeDifference(start);
      
      console.println(duration);      
      return true;
   }
}
