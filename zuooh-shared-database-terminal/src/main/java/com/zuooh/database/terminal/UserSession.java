package com.zuooh.database.terminal;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import com.zuooh.database.Database;
import com.zuooh.database.imdb.Catalog;

public class UserSession {
   
   private static final String DECIMAL_FORMAT = "###,###,###,###.##";

   private final List<String> commands;
   private final MemoryAnalyzer analyzer;
   private final StringBuilder buffer;
   private final DecimalFormat format;
   private final Database database;
   private final Catalog catalog;
   private final UserTime start;
   
   public UserSession(MemoryAnalyzer analyzer, UserTime start, Database database, Catalog catalog) {
      this.format = new DecimalFormat(DECIMAL_FORMAT);
      this.commands = new ArrayList<String>();      
      this.buffer = new StringBuilder();
      this.analyzer = analyzer;
      this.database = database;
      this.catalog = catalog;
      this.start = start;
   }
   
   public UserTime getTime() {
      return start;
   } 
   
   public DecimalFormat getFormat() {
      return format;
   }   
   
   public MemoryAnalyzer getAnalyzer() {
      return analyzer;
   }
   
   public List<String> getHistory(){
      return commands;
   }  
   
   public StringBuilder getBuffer(){
      return buffer;
   }
   
   public Database getDatabase(){
      return database;
   }
   
   public Catalog getCatalog(){
      return catalog;
   }
}
