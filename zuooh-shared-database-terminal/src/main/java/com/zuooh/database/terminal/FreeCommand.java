package com.zuooh.database.terminal;

import java.io.PrintStream;

import com.zuooh.common.memory.MemoryUnit;

public class FreeCommand implements Command {

   @Override
   public boolean execute(UserSession session, PrintStream out, String expression) throws Exception{
      double memoryLimit = Runtime.getRuntime().maxMemory();
      double memoryAllocated = Runtime.getRuntime().totalMemory();
      double memoryFree = Runtime.getRuntime().freeMemory();
      double memoryAvailable = memoryLimit - memoryAllocated;
      double memoryUsed = memoryLimit - (memoryFree + memoryAvailable);
      double percentageUsed = (memoryUsed / memoryLimit) * 100f;
      double percentageAllocated = (memoryAllocated / memoryLimit) * 100f;

      ConsoleTable console = ConsoleTableBuilder.create("memory", "percentage", "size");

      console.add("total", "100%", MemoryUnit.format(memoryLimit));
      console.add("allocated", Math.round(percentageAllocated) + "%", MemoryUnit.format(memoryAllocated));  
      console.add("available", Math.round(100 - percentageUsed) + "%", MemoryUnit.format(memoryLimit - memoryUsed));
      console.add("used", Math.round(percentageUsed) + "%", MemoryUnit.format(memoryUsed)); 
      
      console.draw(out, 20);
      return true;
   }
}
