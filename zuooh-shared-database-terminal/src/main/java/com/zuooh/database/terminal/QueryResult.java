package com.zuooh.database.terminal;

public class QueryResult {

   private final String output;
   private final String memory;
   private final long duration;
   private final int rows;
   
   public QueryResult(String output, String memory, long duration, int rows) {
      this.output = output;
      this.memory = memory;
      this.duration = duration;
      this.rows = rows;
   }

   public String getOutput() {
      return output;
   }

   public String getMemory() {
      return memory;
   }

   public long getDuration() {
      return duration;
   }

   public int getRows() {
      return rows;
   }
}
