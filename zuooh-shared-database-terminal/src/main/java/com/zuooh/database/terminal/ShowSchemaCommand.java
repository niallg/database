package com.zuooh.database.terminal;

import java.io.PrintStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.zuooh.database.Column;
import com.zuooh.database.Schema;
import com.zuooh.database.function.DefaultFunction;
import com.zuooh.database.function.DefaultValue;
import com.zuooh.database.imdb.Catalog;
import com.zuooh.database.imdb.Table;

public class ShowSchemaCommand implements Command {
   
   private static final String COMMAND_PATTERN = "\\s*show\\s+table\\s+(.*)\\s*";

   @Override
   public boolean execute(UserSession session, PrintStream out, String text) throws Exception{
      Catalog catalog = session.getCatalog();
      
      if(catalog != null) {
         Pattern pattern = Pattern.compile(COMMAND_PATTERN, Pattern.CASE_INSENSITIVE);
         Matcher matcher = pattern.matcher(text);
         
         if(!matcher.matches()) {
            throw new IllegalArgumentException("Expression '" + text + "' is invalid");
         }
         String name = matcher.group(1);
         Table table = catalog.findTable(name);
         
         if(table == null) {
            out.println("No table found for '"+name+"'");
         } else {
            Schema schema = table.getSchema();
            ConsoleTable console = ConsoleTableBuilder.create("name", "type", "default");            
            int width = schema.getCount();
            
            for(int i = 0; i < width;i++){
               Column column = schema.getColumn(i);
               DefaultValue value = column.getDefaultValue();
               DefaultFunction function = value.getFunction();
               ConsoleRow row = console.add();
               
               row.set("name", column.getTitle());
               row.set("type", column.getDataType());
               row.set("default", function);
            }
            console.draw(out);
         }
      } else {
         out.println("No catalog found");
      }
      return true;
   }

}
