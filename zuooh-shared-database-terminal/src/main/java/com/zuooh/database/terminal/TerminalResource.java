package com.zuooh.database.terminal;

import java.io.PrintStream;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import com.zuooh.http.resource.Resource;

public class TerminalResource implements Resource {
   
   private final UserTimeParser parser;   
   private final Terminal terminal;
   private final String session;
   private final String command;
   private final String date;
   
   public TerminalResource(Terminal terminal, String command, String date, String session) {
      this.parser = new UserTimeParser();
      this.session = session;
      this.terminal = terminal;
      this.command = command;
      this.date = date;
   }
   
   @Override
   public void handle(Request req, Response resp) throws Exception {
      PrintStream out = resp.getPrintStream(8192);
      String text = req.getParameter(command);
      
      if(command == null) {
         throw new IllegalStateException("Request does not contain a command");
      }
      String instant = req.getParameter(date);
      String user = req.getParameter(session);
      UserTime time = parser.parseTime(instant);               
      
      terminal.submit(out, time, user, text);
   }
}
