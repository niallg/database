package com.zuooh.database.terminal;

import java.util.TimeZone;

public class UserTime {

   private final TimeZone zone;
   private final String text;
   private final long time;   
   
   public UserTime(String text, TimeZone zone, long time) {
      this.zone = zone;
      this.text = text;
      this.time = time;
   }
   
   public TimeZone getTimeZone() {
      return zone;
   }
   
   public long getTime() {
      return time;
   }
   
   public String getText() {
      return text;
   }
   
   @Override
   public String toString() {
      return text;
   }
}

