package com.zuooh.database.terminal;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.zuooh.database.Database;
import com.zuooh.database.DatabaseConnection;
import com.zuooh.database.Record;
import com.zuooh.database.ResultIterator;
import com.zuooh.database.Statement;
import com.zuooh.database.sql.Query;
import com.zuooh.database.sql.Verb;

public class QueryExecutor {

   private final int limit;

   public QueryExecutor() {
      this(5000);
   }
   
   public QueryExecutor(int limit) {
      this.limit = limit;
   }
 
   public QueryResult execute(UserSession session, QueryRequest request) throws Exception {
      ResultFormatter extractor = new ResultFormatter(session, request);
      ByteArrayOutputStream buffer = new ByteArrayOutputStream();
      PrintStream out = new PrintStream(buffer);      
      List<Record> results = new ArrayList<Record>();
      
      try {        
         Query query = request.getQuery();
         Verb verb = query.getVerb();
         String source = query.getSource();
         Database database = session.getDatabase();
         DatabaseConnection connection = database.getConnection();
         MemoryAnalyzer analyzer = session.getAnalyzer();
         long start = System.currentTimeMillis();
         int repeat = request.getRepeat();
         int rows = 0;
         
         if(verb == Verb.SELECT || verb == Verb.SELECT_DISTINCT) {
            analyzer.start();
            
            try {
               Statement statement = connection.prepareStatement(source);
               ResultIterator<Record> iterator = statement.execute();
            
               while (iterator.hasMore()) {
                  Record record = iterator.next();
                  results.add(record);
               }
            } finally {
               analyzer.stop();
            }
            if (results.isEmpty()) {
               out.println("No results");
            } else {
               ConsoleTable console = null;

               for (Record record : results) {
                  Set<String> titles = record.getColumns();
                  
                  if (console == null) {
                     console = ConsoleTableBuilder.create(titles);
                  }
                  Map<String, String> values = extractor.format(record);
                  ConsoleRow row = console.add();

                  for (String title : titles) {
                     String value = values.get(title);
                     row.set(title, value);
                  }
                  if (rows > limit) { // don't allow it to go crazy!!
                     break;
                  }
                  rows++;
               }
               console.draw(out);
               out.close();
            }
         } else {
            analyzer.start();
            
            try {
               connection.executeStatement(source);
            } finally {
               analyzer.stop();
            }            
         }
         if (repeat > 1) {
            for (int i = 1; i < repeat; i++) {
               connection.executeStatement(source);
            }
         }
         String result = buffer.toString();
         String memory = analyzer.change();
         long finish = System.currentTimeMillis();
         long duration = finish - start;
         
         return new QueryResult(result, memory, duration, rows);
      } catch (Exception e) {
         throw new IllegalStateException("Could not execute " + request, e);
      }
   }

}
