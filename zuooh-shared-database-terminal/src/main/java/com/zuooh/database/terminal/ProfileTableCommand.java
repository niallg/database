package com.zuooh.database.terminal;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.zuooh.database.Column;
import com.zuooh.database.Schema;
import com.zuooh.database.data.DataType;
import com.zuooh.database.imdb.Catalog;
import com.zuooh.database.imdb.Cell;
import com.zuooh.database.imdb.Row;
import com.zuooh.database.imdb.Table;
import com.zuooh.database.imdb.TableModel;
import com.zuooh.database.imdb.filter.Filter;
import com.zuooh.database.imdb.filter.NoFilter;

public class ProfileTableCommand implements Command {
   
   private static final String COMMAND_PATTERN = "\\s*profile\\s+table\\s+(.*)\\s*";
   
   private final Map<String, ColumnProfiler> profilers;
   private final Filter filter;
   
   public ProfileTableCommand() {
      this.profilers = new HashMap<String, ColumnProfiler>();
      this.filter = new NoFilter();
   }

   @Override
   public boolean execute(UserSession session, PrintStream out, String text) throws Exception{
      Catalog catalog = session.getCatalog();
      
      if(catalog != null) {
         Pattern pattern = Pattern.compile(COMMAND_PATTERN, Pattern.CASE_INSENSITIVE);
         Matcher matcher = pattern.matcher(text);
         
         if(!matcher.matches()) {
            throw new IllegalArgumentException("Expression '" + text + "' is invalid");
         }
         String name = matcher.group(1);
         Table table = catalog.findTable(name);
         
         if(table == null) {
            out.println("No table found for '" + name + "'");
         } else {
            Schema schema = table.getSchema();
            TableModel model = table.getModel();
            List<String> titles = schema.getColumns();
            List<Row> rows = model.list(filter);
            
            for(Row row : rows) {
               for(String title : titles) {
                 Column column = schema.getColumn(title);
                 ColumnProfiler profiler = profilers.get(title);
                 
                 if(profiler == null) {
                    profiler = new ColumnProfiler(column);
                    profilers.put(title, profiler);
                 }
                 DataType type = column.getDataType();
                 int index = column.getIndex();
                 
                 if(type == DataType.TEXT || type == DataType.SYMBOL) {
                    Cell cell = row.getCell(index);
                    String value = (String)cell.getValue();
                    
                    profiler.profile(value);
                 }                 
               }
            }   
            if(!profilers.isEmpty()) {
               ConsoleTable console = ConsoleTableBuilder.create("column", "duplicates");            
               Set<String> keys = profilers.keySet();
               
               for(String key : keys) {
                  ColumnProfiler profiler = profilers.get(key);
                  ConsoleRow row = console.add();
                  int count = profiler.count();
                  
                  row.set("column", key);
                  row.set("duplicates", count);
               }
               console.draw(out);
            } else {
               out.println("Table contains no strings");
            }
         }
      } else {
         out.println("No catalog found");
      }
      return true;
   }
   
   private class ColumnProfiler {
      
      private final Map<String, List<String>> duplicates;
      private final Column column;
      
      public ColumnProfiler(Column column) {
         this.duplicates = new HashMap<String, List<String>>();
         this.column = column;
      }
      
      public int count() {
         Set<String> keys = duplicates.keySet();
         int count = 0;
         
         for(String key : keys) {
            List<String> values = duplicates.get(key);
            int size = values.size();
            
            if(size > 1) { // don't include the one original value
               count += size;
            }
         }
         return count;
      }
      
      public void profile(String value) {
         if(value != null) {
            List<String> values = duplicates.get(value);
            
            if(values == null) {
               values = new ArrayList<String>();
               duplicates.put(value, values);
               values.add(value);
            }
            int count = values.size();
            
            for(int i = 0; i < count; i++) {
               String next = values.get(i);
               
               if(next == value) { // if its an identity match only!!!
                  break;
               }
               if(i + 1 == count) {
                  values.add(value);  // same string different instance
               }
            }
         }
      }
   }

}
