package com.zuooh.database.terminal;

import java.io.PrintStream;
import java.util.concurrent.Executor;

import com.zuooh.database.Database;
import com.zuooh.database.imdb.Catalog;

public class Terminal {

   private final CommandInterpreter interpreter;
   private final Executor executor;
   
   public Terminal(Database database, Catalog catalog, Executor executor){
      this.interpreter = new CommandInterpreter(database, catalog);
      this.executor = executor;
   }
   
   public void submit(PrintStream console, UserTime time, String user, String text) throws Exception {
      UserCommand command = new UserCommand(console, time, user, text);
      executor.execute(command);
   }
   
   private class UserCommand implements Runnable {
      
      private final PrintStream console;
      private final UserTime time;
      private final String name;
      private final String command;     
      
      public UserCommand(PrintStream console, UserTime time, String name, String command) {
         this.console = console;
         this.name = name;
         this.command = command;
         this.time = time;
      }
      
      @Override
      public void run() {
         try {
            interpreter.execute(console, time, name, command);
         }catch(Exception cause){
            cause.printStackTrace(console);
         }finally{
            console.close();
         }
      }
      
   }
}
