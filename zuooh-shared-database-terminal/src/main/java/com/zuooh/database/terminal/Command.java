package com.zuooh.database.terminal;

import java.io.PrintStream;

public interface Command {
   boolean execute(UserSession session, PrintStream console, String expression) throws Exception;
}
