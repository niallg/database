package com.zuooh.database.terminal;

import java.io.PrintStream;

import com.zuooh.common.collections.Cache;
import com.zuooh.common.collections.LeastRecentlyUsedCache;
import com.zuooh.database.Database;
import com.zuooh.database.imdb.Catalog;

public class CommandInterpreter {
   
   private final Cache<String, UserSession> sessions;
   private final MemoryAnalyzer analyzer;
   private final Database database;
   private final Catalog catalog;
   
   public CommandInterpreter(Database database, Catalog catalog){
      this.sessions = new LeastRecentlyUsedCache<String, UserSession>();
      this.analyzer = new MemoryAnalyzer(); // creates thread locals
      this.database = database;
      this.catalog = catalog;
   }
   
   public void execute(PrintStream console, UserTime time, String user, String text) throws Exception {
      UserSession session = sessions.fetch(user);
      
      if(session == null) {
         session = new UserSession(analyzer, time, database, catalog);
         sessions.cache(user, session);
      }
      StringBuilder buffer = session.getBuffer();
      Command command = null;
      
      if(text.equalsIgnoreCase("history") || text.equalsIgnoreCase("show history")) {
         command = new ShowHistoryCommand();
      } else if(text.equalsIgnoreCase("show tables")) {
         command = new ShowCatalogCommand();
      } else if(text.toLowerCase().startsWith("show table ")) {
         command = new ShowSchemaCommand();
      } else if(text.toLowerCase().startsWith("profile table ")) {
         command = new ProfileTableCommand();         
      } else if(text.equalsIgnoreCase("go")) {
         command = new GoCommand();
      } else if(text.equalsIgnoreCase("free") || text.equalsIgnoreCase("show memory")) {
         command = new FreeCommand();
      } else if(text.equalsIgnoreCase("top") || text.equalsIgnoreCase("show threads")) {
         command = new TopCommand();         
      } else if(text.equalsIgnoreCase("uptime")) {         
         command = new UpTimeCommand();
      } else if(text.startsWith("gc")) {
         command = new GarbageCollectCommand();          
      } else if(text.startsWith("!")) {
         command = new RepeatCommand();         
      } else {         
         buffer.append(text);
         buffer.append(" ");
      }
      if(command != null){
         command.execute(session, console, text);
         buffer.setLength(0);
      }
   }      
   
}